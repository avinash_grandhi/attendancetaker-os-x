//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


var optionalString: String? = "Hello"
print(optionalString == nil)

var optionalName: String? = "Johnny Appleseed"
var greeting = "Hello!"
if let name = optionalName {
    greeting = "Hello, \(name)"
}

print(greeting)