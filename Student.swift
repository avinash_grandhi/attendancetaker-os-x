//
//  Student.swift
//  AttendanceTakerOSX
//
//  Created by admin on 7/23/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import Cocoa
//import CommonCrypto
//import CommonCrypto

/****************************************************************************
This class represents a Student.
*************************************************************************** */
class Student: NSView {
    
    var name : NSString!
    var duplicateView : StudentDuplicate!
    var imageSelected = false
    var textSelected = false
    // This will be the first object in the responder chain to be sent key events and action messages.
    var entered = 1
    var selected : Bool = false // To check if the desk is selected or not?
    var startingPanLocation : CGPoint!
    var attendance_present : Int!
    var attendance_absent : Int!
    var attendance_late : Int!
    var mainSplitVCDelegate : MainSplitVC!
    var image = NSImageView() // Image of the student.
    var nameTF = NSTextField() //Name of the Student.
    var classDisplayVCDelegate : ClassDisplayVC!
    var notes: NSString =  "" // notes for the student
    var sid : NSString!//sid of the student
    var surname : NSString!
    var prototype : NSButtonCell!
    var radio : NSMatrix!
    
    var attendanceTotal  : Int = 0
    var presentTotal : Int = 0
    var absentTotal : Int = 0
    var lateTotal : Int = 0
    
    
    /****************************************************************************
    Instantiating the view based on the frame value and adding new image if it is newCell
    *************************************************************************** */
    init(frame frameRect: NSRect, newCell : Bool, name : NSString, sid : NSString){
        super.init(frame: frameRect)
        let frame = NSMakeRect(0, 0, 80, 80)
        duplicateView = StudentDuplicate(frame: frame )
        let myPanGR:NSPanGestureRecognizer = NSPanGestureRecognizer(target: self, action: "moveMe:")
        self.addGestureRecognizer(myPanGR)
        self.sid = sid
        loadImage()
        if name != ""{
            loadName(name)
            self.name = name
        }
        if newCell == true{
            loadNewImage()
        }
        
        radioButtons()
        
        loadDuplicateView()
        
        
        
    }
    
    /****************************************************************************
    Loading the mickey image to the desk view
    *************************************************************************** */
    func loadImage(){
        image.setFrameOrigin(NSMakePoint(5, 5))
        
        
        
        if (sid == "noSid"){
            image.setFrameSize(NSMakeSize(40, 40))
            image.image = NSImage(named: "Mickey_Mouse.png")
            
        }
        else{
            image.setFrameSize(NSMakeSize(55, 55))
            var digest = md5(self.sid.lowercaseString)
            if (sid.length == 7){
                 digest = md5(self.sid.stringByAppendingString("@mail.nwmissouri.edu").lowercaseString)
            }
            
            
            image.image = NSImage(contentsOfURL : NSURL(string: "https://www.gravatar.com/avatar/\(digest)")!)
            
        }
        
        image.editable = false
        //        var event = NSEvent()
        //        var num =  event.keyCode
        //        image.keyDown(event){}
        self.addSubview(image)
    }
    
    func returnName() -> NSString {
        return self.name
    }
    
    /****************************************************************************
    Loading the new sticker image to the desk view
    *************************************************************************** */
    
    func loadNewImage(){
        let image = NSImageView()
        image.setFrameOrigin(NSMakePoint(0, 30))
        image.setFrameSize(NSMakeSize(50, 50))
        image.image = NSImage(named: "new_red.png")
        
        
        
        self.addSubview(image)
    }
    
    
    
    func radioButtons(){
        
        prototype = NSButtonCell()
        prototype.title = ""
        prototype.image = NSImage(named : "Disabled.png")
        prototype.alternateImage = NSImage(named : "Present1.png")
        prototype.setButtonType(NSButtonType.RadioButton)
        prototype.imageDimsWhenDisabled = true
        radio = NSMatrix(frame: NSRect(x: 60, y: 1 , width: 250, height: 55), mode: NSMatrixMode.RadioModeMatrix, prototype: prototype, numberOfRows: 3, numberOfColumns: 1)
        let first = radio.cellAtRow(0, column: 0) as! NSButtonCell
        first.action = Selector("radioButtonChangedFunc")
        first.target = self
        let second = radio.cellAtRow(1, column: 0) as! NSButtonCell
        second.title = ""
        second.image = NSImage(named: "Disabled.png")
        second.alternateImage = NSImage(named : "Cross.png")
        let third = radio.cellAtRow(2, column: 0) as! NSButtonCell
        third.title = ""
        third.image = NSImage(named : "Disabled.png")
        third.alternateImage = NSImage(named: "Late.png")
        // prototype.performClick("mouseDown1")
        //prototype.target = Selector(performClick(self))
        second.action = Selector("radioButtonChangedFunc")
        second.target = self
        
        third.action = Selector("radioButtonChangedFunc")
        third.target = self
        // prototype.add
        //prototype.action = NSSelecto
        //second.action = Selector()
        self.addSubview(radio)
    }
    
    
    
    
    /****************************************************************************
    Loads the name of the Student
    *************************************************************************** */
    func loadName(name : NSString){
        nameTF.setFrameOrigin(NSMakePoint(5, 55))
        nameTF.setFrameSize(NSMakeSize(70, 20))
        nameTF.stringValue = name.description
        nameTF.continuous = true
        nameTF.editable = false
        nameTF.backgroundColor = NSColor.orangeColor()
        self.addSubview(nameTF)
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func loadDuplicateView(){
        self.setNeedsDisplayInRect(self.frame)
        self.addSubview(duplicateView)  //, positioned: NSWindowOrderingMode.Above, relativeTo: self)
        // self.subviews[3].becomeFirstResponder()
        
    }
    /****************************************************************************
    overriding the drawrect function and filling the frame with color
    *************************************************************************** */
    
    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)
        let myBP:NSBezierPath = NSBezierPath(rect: NSMakeRect(5.0, 5.0, self.frame.size.width-10, self.frame.size.height-10))
        NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).setFill()
        myBP.fill()
        
    }
    
    /****************************************************************************
    Adding the panGestureRecogniser to the Desk
    *************************************************************************** */
    
    func moveMe(myPanGR:NSPanGestureRecognizer){
        let deskIV = myPanGR.view!
        if myPanGR.state == NSGestureRecognizerState.Began {
            startingPanLocation = deskIV.frame.origin
        }
        deskIV.frame.origin.x = startingPanLocation.x + myPanGR.translationInView(self.superview).x
        deskIV.frame.origin.y = startingPanLocation.y + myPanGR.translationInView(self.superview).y
    }
    
    /****************************************************************************
    Handling keyboard events. (Delete == 51)
    *************************************************************************** */
    
    override func keyDown(theEvent: NSEvent) {
        if theEvent.keyCode == 51 {
            self.removeFromSuperview()
            (self.mainSplitVCDelegate.splitViewItems[2]).collapsed = true
        }
    }
    
    /****************************************************************************
    Handling left mouse click. Toggle to specify if a desk is selected or not
    *************************************************************************** */
    
    override func mouseDown(theEvent: NSEvent) {
        
        NSNotificationCenter.defaultCenter().postNotificationName("unhighlightOtherStudents", object: self)
        self.layer?.backgroundColor = NSColor.redColor().CGColor
        (self.mainSplitVCDelegate.splitViewItems[2] ).collapsed = false
        let studentVC = (self.mainSplitVCDelegate.splitViewItems[2] ).viewController as! studentPopUpDetailVC
        studentVC.studentName.stringValue = self.name.description
        studentVC.notesTV.string = self.notes.description
        studentVC.presentText.stringValue = self.presentTotal.description
        studentVC.absentText.stringValue = self.absentTotal.description
        studentVC.lateText.stringValue = self.lateTotal.description
        studentVC.totalText.stringValue = self.attendanceTotal.description
        studentVC.sidText.stringValue = self.sid.description
        studentVC.studentSurname.stringValue = self.surname.description
        studentVC.studentDeligate = self
        
        if (sid == "noSid"){
            
            studentVC.studentImage.image = NSImage(named: "Mickey_Mouse.png")
            
        }
        else{
            
            
            
            studentVC.studentImage.image =  image.image            
        }
        
        
        self.classDisplayVCDelegate.selectedStudent = self
        //studentVC
    }
    
    
    func radioButtonChangedFunc(){
        self.classDisplayVCDelegate.stausLabel.stringValue = "Modified"
        mouseDown(NSEvent())
    }
    
    //   func takeAttendance()-> Int{
    //    return radio.selectedRow
    //    }
    
    
    //    function to implement md5 generation
    
    func md5(string : String) -> String {
        var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
        if let data = string.dataUsingEncoding(NSUTF8StringEncoding) {
            CC_MD5(data.bytes, CC_LONG(data.length), &digest)
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex
    }
    
    func updateImage(){
        
        self.subviews[0].removeFromSuperview()
        loadImage()
        
      self.subviews[0].removeFromSuperview()
        loadName(self.name)
        let studentVC = (self.mainSplitVCDelegate.splitViewItems[2] ).viewController as! studentPopUpDetailVC
        if (sid == "noSid"){
            
            studentVC.studentImage.image = NSImage(named: "Mickey_Mouse.png")
            
        }
        else{
            
            
            
            studentVC.studentImage.image =  image.image
        }
        
    }
    
}
