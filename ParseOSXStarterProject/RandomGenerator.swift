//
//  RandomMaker.swift
//  AttendanceTakerOSX
//
//  Created by admin on 10/24/15.
//  Copyright © 2015 Parse. All rights reserved.
//

import Cocoa
import Foundation

class RandomGenerator: NSObject {
    
    
    
    
    
        var classDisplayVCDelegate : ClassDisplayVC!
        
    var deskSize : CGFloat = 80.0
    
    // no of groups
    
    var noOfGroups = 0
    
    //no of desks in a row
    var noOfDeskainGroup = 0
    
        func loadGroupsContainer(){
            
            
            let contentView : NSView = NSView()
            
            let totalNoOfDesks = classDisplayVCDelegate.classData.namesArray.count
            contentView.setFrameOrigin(NSMakePoint(0, 0))
            
            (self.classDisplayVCDelegate.mainSplitView.splitViewItems[2] ).collapsed = true
            
            
            
            //no of desks in a column
            let groupNoOfRows = ceil(sqrt(Double(noOfDeskainGroup)))
            var groupNoOfColumns = floor(sqrt(Double(noOfDeskainGroup)))
            Swift.print("value is \(groupNoOfRows * groupNoOfColumns) and noOfdesks \(noOfDeskainGroup)")
            if Int(groupNoOfRows * groupNoOfColumns ) < Int(noOfDeskainGroup){
                groupNoOfColumns += 1
            }
            
            //calculating group width and height
            
            let groupWidth = CGFloat(groupNoOfRows) * (deskSize + CGFloat(10))
            let groupHeight = CGFloat(groupNoOfColumns) * (deskSize + CGFloat(10))
            
            Swift.print("noOfRows : \(groupNoOfRows)  noOfcols : \(groupNoOfColumns)")
            
            var totalNoOfColums = 0
            var totalNoOfRows = 0
            
            //calculate totalNooFRows and Colums
            if self.classDisplayVCDelegate.contentScrollView.contentView.frame.width > groupWidth * CGFloat(noOfGroups){
                totalNoOfColums = noOfGroups
                totalNoOfRows = 1
            }
            else{
                let capacity = Int(self.classDisplayVCDelegate.contentScrollView.contentView.frame.width / groupWidth)
                totalNoOfColums = capacity
                totalNoOfRows = 0
                var required = noOfGroups
                while(required>0){
                    required -= capacity
                    totalNoOfRows++
                }
            }
            
          //  Swift.print("total group rows : \(totalNoOfRows) no of cols is \(totalNoOfColums)")
            
            
            //calcuate Total width and height
            
            let mainStaringX = 0
            let mainStartingY = 0
            var groupStaringX = mainStaringX
            var groupStaringY = mainStartingY
            
//            let totalWidth = groupWidth * CGFloat(totalNoOfColums)
//            let totalHeight = groupHeight * CGFloat(totalNoOfRows)
            
            contentView.setFrameSize(NSMakeSize(groupWidth * CGFloat(noOfGroups) + 100.0, groupHeight * CGFloat(noOfGroups) + 100.0))
            var count = 0
            var groupCount = 0
            for i in 0..<totalNoOfRows{
                groupStaringX = mainStaringX
                groupStaringY = Int((groupHeight + 50.0) * CGFloat(i) )
                for mainCol in 0..<totalNoOfColums{
                   // groupStaringY = mainStartingY + Int(groupHeight) - 30
                    
                     Swift.print("row is \(i) column is \(mainCol)")
                    groupCount = 0
                    //code for group
                    for groupRow in 0..<Int(groupNoOfRows){
                        
                        if groupCount == noOfDeskainGroup{
                            groupCount = 0
                            break
                        }
                        for groupCol in 0..<Int(groupNoOfColumns){
                            
                            
                            if (count == totalNoOfDesks){
                                break
                            }
                            if groupCount == noOfDeskainGroup {
                                groupCount = 0
                                
                               break
                            }else{
                            groupCount++
                               
                            let frame =  NSMakeRect( CGFloat(groupStaringX + groupCol * Int(deskSize) + (Int(groupWidth) * mainCol) ), CGFloat(Int( groupStaringY) + groupRow * Int(deskSize) ) , deskSize, deskSize)
                            //Swift.print(count)
                           let student = Student(frame: frame, newCell :false,name : self.classDisplayVCDelegate.classData.namesArray[count],sid: self.classDisplayVCDelegate.classData.sidArray[count])
                            //student.layer!.backgroundColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).CGColor
                            student.layer?.backgroundColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).CGColor
                            student.notes = self.classDisplayVCDelegate.classData.notesArray[count++]
                            student.classDisplayVCDelegate = self.classDisplayVCDelegate
                            student.mainSplitVCDelegate = self.classDisplayVCDelegate.mainSplitView
                            contentView.addSubview(student)
                            }
                            Swift.print(" done here")
                            
                        }
                    }
                    
                }
                
                
            }
            
            
            
            self.classDisplayVCDelegate.contentScrollView.documentView = contentView
            
            
          
            
            
            
            
            
            //            // The container width and height of the desks if they were seen as placed in rectangle
            //            var desksWidth = (maxXCoordinate - minXCoordinate) + deskSize
            //            var desksHeight = maxYcoordinate - minYCoordinate + deskSize
            //
            //            shiftDifferenceWidth  = 0
            //            shiftDifferenceHeight  = 0
            //
            //            // calculating the deskwidth and shiftWidthDifference
            //            if self.contentScrollView.contentView.frame.width > desksWidth{
            //                desksWidth = self.contentScrollView.contentView.frame.width
            //
            //                shiftDifferenceWidth = ((desksWidth - (maxXCoordinate + deskSize)) - (minXCoordinate))/2
            //
            //
            //            }
            //            else{
            //                shiftDifferenceWidth =  (minXCoordinate * -1)
            //            }
            //
            //
            //            // calculating the deskheight and shiftHeightDifference
            //            if (self.contentScrollView.contentView.frame.height ) > desksHeight{
            //                desksHeight = self.contentScrollView.contentView.frame.height
            //                shiftDifferenceHeight = ((desksHeight - (maxYcoordinate + deskSize)) - (minYCoordinate))/2
            //            }
            //            else{
            //                shiftDifferenceHeight = (minYCoordinate * -1)
            //            }
            //
            //
            //            contentview.setFrameSize(NSMakeSize(desksWidth, desksHeight))
            //
            //
            //            for i in 0..<(sender as! Class).deskXaxis.count{
            //
            //                let frame = NSMakeRect(classData.deskXaxis[i] + shiftDifferenceWidth , classData.deskYaxis[i] + shiftDifferenceHeight, deskSize, deskSize)
            //
            //
            //                name = (sender as! Class).namesArray[i]
            //                sid = (sender as! Class).sidArray[i]
            //                notes = (sender as! Class).notesArray[i]
            //
            //
            //                student = Student(frame: frame, newCell :false,name : name,sid: sid)
            //                //student.layer!.backgroundColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).CGColor
            //                student.layer?.backgroundColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).CGColor
            //                student.notes = notes
            //                student.classDisplayVCDelegate = self
            //                student.mainSplitVCDelegate = mainSplitView
            //                contentview.addSubview(student)
            //                //                self.selectedStudent = student
            //            }
            //            contentScrollView.documentView = contentview
            //            documentLayer  = (contentScrollView.documentView! as! NSView).layer!
            //            documentLayer.backgroundColor = CGColorCreateGenericRGB(0.8, 8, 1, 1)
            //        }
            //        //  loadAttendance()
            //        datePickerDate.dateValue = NSDate()
            //        Swift.print(NSDate())
            //        loadTotalAttendance()
        }
        
    }

    
    


