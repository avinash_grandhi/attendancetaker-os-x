//
//  ClassDisplayVC.swift
//  AttendanceTakerOSX
//
//  Created by admin on 7/23/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import Cocoa
import ParseOSX

/****************************************************************************
This ViewController is instantiated when the user selects any class from classes table view. *************************************************************************** */

class ClassDisplayVC: NSViewController {
    
    /* ****************************************************************************
    
    DetailVC(TabViewController)
    \
    |---------|--\------------------|
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|    ClassDisplayVC    |
    |      - +|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |      - +|                     |
    |---------|---------------------|
    
    
    
    ****************************************************************************
    
    Variables for the ClassDisplayVC
    
    *************************************************************************** */
    
    
    // reference to the MainSplitVC
    
    // @IBAction func fal(sender: AnyObject) {
    // }
    var student : Student! //Each Student in the view.
    var mainSplitView : MainSplitVC!
    var classData : Class!
    //Co-ordinates for the new Student.
    var newDeskAddPosition_x : CGFloat = 150
    var newDeskAddPosition_y : CGFloat = 250
    //Size of the desk.
    var deskSize: CGFloat  = 80
    
    @IBOutlet weak var saveBTN: NSButton!
    
    
    @IBOutlet weak var addBTN: NSButton!
    // The below four variables are used to re-arrange the Students when window's size changes.
    var maxXCoordinate: CGFloat!
    var minXCoordinate : CGFloat!
    var minYCoordinate : CGFloat!
    var maxYcoordinate : CGFloat!
    
    
    var name : NSString = ""
    var sid : NSString = ""
    var notes: NSString = ""
    var surname : NSString = ""
    
    
    var shiftDifferenceWidth : CGFloat!
    var shiftDifferenceHeight : CGFloat!
    
    
    //variables for rectangle frame for selection
    var startPoint : NSPoint!
    var endPoint : NSPoint!
    var documentLayer : CALayer!
    var shapeLayer : CAShapeLayer!
    
    
    var selectedStudent : Student!// the highlighted student
    
    @IBOutlet weak var contentScrollView: NSScrollView!
    
    
    @IBOutlet weak var datePickerDate: NSDatePickerCell!
    
    @IBOutlet weak var stausLabel: NSTextFieldCell!
    
    
    @IBOutlet weak var classOrGroupSegment: NSSegmentedControl!
    
    @IBAction func classOrGroupSegmentFunc(sender: NSSegmentedControl) {
        
        if (sender.selectedSegment == 1){
            
            self.presentViewControllerAsSheet(self.storyboard?.instantiateControllerWithIdentifier("addRandomGroups") as! AddRandomGroups)
            
//           // loadGroupsContainer()
//            let random = RandomGenerator()
//            random.classDisplayVCDelegate = self
//            random.loadGroupsContainer()
            
            
        }
        else if (sender.selectedSegment == 0){
            loadContainer(self.classData)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "unhighlightOtherStudents:", name: "unhighlightOtherStudents", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("classWindowResize:"), name:NSViewFrameDidChangeNotification , object: nil);
        
      
        
    }
    
    
    func classWindowResize(notification : NSNotification){
        if notification.object as! NSView == contentScrollView{
            if classData != nil{
                updateMaxMinFrameBasedOnContentView()
                updateViewShiftDifference()
                
            }
        }
    }
    
    
    
    @IBAction func dateChangeFunc(sender: AnyObject) {
        loadAttendance()
        
    }
    
    
    @IBAction func exportDataFunc(sender: AnyObject) {
        
        let savePanel = NSSavePanel()
        
        savePanel.nameFieldStringValue = "results.csv"
        
        savePanel.runModal()
        
        var totalData = ""
        for view in contentScrollView.documentView!.subviews as! [Student]{
            
            Swift.print(view.name)
            Swift.print(view.presentTotal)
            Swift.print(view.absentTotal)
            Swift.print(view.attendanceTotal)
            
           let resultLine = NSString(string: "\(view.name),\(view.sid),\(view.attendanceTotal),\(view.presentTotal),\(view.absentTotal),\(view.lateTotal),\(view.attendanceTotal) \n")
            totalData += resultLine.description
            do {
           
          
            
            let path = savePanel.URL?.path
            
                try
                totalData.writeToFile(path!, atomically: true, encoding: NSUTF8StringEncoding) }
            
            
            catch {
                Swift.print("Not Saved")
            }
            
        }
        
    }
    
    
    
    /****************************************************************************
    
    
    Scroll View
    |---------|--------------------/-
    |---------|                _    |
    |---------|               |_|---|----->Desk can be considered as top right point of the illusioned rectangle of desks
    |---------|                     |
    |---------|     _               |
    |---------|    |_|              |
    |      - +|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|     _               |
    |---------|    |_|--------------|----->Desk can be considered as bottom left point of the illusioned rectangle of desks
    |      - +|                     |
    |---------|---------------------|
    
    
    ****************************************************************************
    Updates the desks rectangle(illusion) max and min values based on classDetails data
    ****************************************************************************/
    
    
    func updateMaxMinFrameBasedOnRoomData(classData: Class){
        if classData.deskXaxis.count > 0{
            maxXCoordinate = classData.deskXaxis[0]
            minXCoordinate = classData.deskXaxis[0]
            minYCoordinate = classData.deskYaxis[0]
            maxYcoordinate = classData.deskYaxis[0]
        }
        for i in 0..<classData.deskXaxis.count{
            if maxXCoordinate < classData.deskXaxis[i]{
                maxXCoordinate = classData.deskXaxis[i] as CGFloat
            }
            if minXCoordinate > classData.deskXaxis[i]{
                minXCoordinate = classData.deskXaxis[i] as CGFloat
            }
            
            if maxYcoordinate < classData.deskYaxis[i]{
                maxYcoordinate = classData.deskYaxis[i] as CGFloat
            }
            if minYCoordinate > classData.deskYaxis[i]{
                minYCoordinate = classData.deskYaxis[i] as CGFloat
            }
            
        }
        
    }
    
    
    func updateMaxMinFrameBasedOnContentView(){
        if contentScrollView.documentView?.subviews.count > 0{
            maxXCoordinate = (contentScrollView.documentView!.subviews[0] as! Student).frame.origin.x
            maxYcoordinate = (contentScrollView.documentView!.subviews[0] as! Student).frame.origin.y
            minXCoordinate = (contentScrollView.documentView!.subviews[0] as! Student).frame.origin.x
            minYCoordinate = (contentScrollView.documentView!.subviews[0] as! Student).frame.origin.y
            let students = (contentScrollView.documentView!.subviews) as! [Student]
            for student in students{
                if maxXCoordinate < student.frame.origin.x{
                    maxXCoordinate = student.frame.origin.x as CGFloat
                }
                if minXCoordinate > student.frame.origin.x{
                    minXCoordinate = student.frame.origin.x as CGFloat
                }
                
                if maxYcoordinate < student.frame.origin.y{
                    maxYcoordinate = student.frame.origin.y as CGFloat
                }
                if minYCoordinate > student.frame.origin.y{
                    minYCoordinate = student.frame.origin.y as CGFloat
                }
                
                
                
            }
        }
    }
    
    
    /* ****************************************************************************
    
    
    --------------------------|
    |                  <----->|
    |           RightDiffernce|
    |        ________         |
    |       |        |        |
    |       |        |        |
    |       |     ---|--------|---------->Desk views inside the illusioned rectangle
    |       |________|        |   |
    |LeftDiffernce            |
    |<---->             ------|---------->Scroll content view
    |_________________________|
    
    
    shiftDiffereceWidth = diffence of (leftDiffence and RightDifference)
    ****************************************************************************
    Get the shiftDifferenceWidth and shiftDifferenceHeight
    Load the student views in the center of the scrollView using shiftDifferences
    Student origin cooordinates are retrived from the calling fucntion
    *************************************************************************** */
    
    
    func loadContainer(sender : AnyObject){
        if sender.isKindOfClass(Class){
            
            classData = sender as! Class
            updateMaxMinFrameBasedOnRoomData(classData)
            
            let contentview = NSView()
            contentview.setFrameOrigin(NSMakePoint(0, 0))
            
            (self.mainSplitView.splitViewItems[2] ).collapsed = true
            // The container width and height of the desks if they were seen as placed in rectangle
            var desksWidth = (maxXCoordinate - minXCoordinate) + deskSize
            var desksHeight = maxYcoordinate - minYCoordinate + deskSize
            
            shiftDifferenceWidth  = 0
            shiftDifferenceHeight  = 0
            
            // calculating the deskwidth and shiftWidthDifference
            if self.contentScrollView.contentView.frame.width > desksWidth{
                desksWidth = self.contentScrollView.contentView.frame.width
                
                shiftDifferenceWidth = ((desksWidth - (maxXCoordinate + deskSize)) - (minXCoordinate))/2
                
                
            }
            else{
                shiftDifferenceWidth =  (minXCoordinate * -1)
            }
            
            
            // calculating the deskheight and shiftHeightDifference
            if (self.contentScrollView.contentView.frame.height ) > desksHeight{
                desksHeight = self.contentScrollView.contentView.frame.height
                shiftDifferenceHeight = ((desksHeight - (maxYcoordinate + deskSize)) - (minYCoordinate))/2
            }
            else{
                shiftDifferenceHeight = (minYCoordinate * -1)
            }
            
            
            contentview.setFrameSize(NSMakeSize(desksWidth, desksHeight))
            
            
            for i in 0..<(sender as! Class).deskXaxis.count{
                
                let frame = NSMakeRect(classData.deskXaxis[i] + shiftDifferenceWidth , classData.deskYaxis[i] + shiftDifferenceHeight, deskSize, deskSize)
                
                
                name = (sender as! Class).namesArray[i]
                sid = (sender as! Class).sidArray[i]
                notes = (sender as! Class).notesArray[i]
                surname = (sender as! Class).surnameArray[i]
                
                
                student = Student(frame: frame, newCell :false,name : name,sid: sid)
                student.surname = surname
                //student.layer!.backgroundColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).CGColor
                student.layer?.backgroundColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).CGColor
                student.notes = notes
                student.classDisplayVCDelegate = self
                student.mainSplitVCDelegate = mainSplitView
                contentview.addSubview(student)
                //                self.selectedStudent = student
            }
            contentScrollView.documentView = contentview
            documentLayer  = (contentScrollView.documentView! as! NSView).layer!
            documentLayer.backgroundColor = CGColorCreateGenericRGB(0.8, 8, 1, 1)
        }
      //  loadAttendance()
        datePickerDate.dateValue = NSDate()
        Swift.print(NSDate())
        loadTotalAttendance()
    }
    
    
    
    //updating the contentview size and calculating the shift difference
    
    func updateViewShiftDifference(){
        let contentview = NSView()
        contentview.setFrameOrigin(NSMakePoint(0, 0))
        
        // The container width and height of the desks if they were seen as placed in rectangle
        var desksWidth = (maxXCoordinate - minXCoordinate) + deskSize
        var desksHeight = maxYcoordinate - minYCoordinate + deskSize
        
        shiftDifferenceWidth = 0
        shiftDifferenceHeight  = 0
        
        // calculating the deskwidth and shiftWidthDifference
        if self.contentScrollView.contentView.frame.width > desksWidth{
            desksWidth = self.contentScrollView.contentView.frame.width
            
            shiftDifferenceWidth = ((desksWidth - (maxXCoordinate + deskSize)) - (minXCoordinate))/2
            
            
        }
        else{
            shiftDifferenceWidth =  (minXCoordinate * -1)
        }
        
        
        // calculating the deskheight and shiftHeightDifference
        if (self.contentScrollView.contentView.frame.height ) > desksHeight{
            desksHeight = self.contentScrollView.contentView.frame.height
            shiftDifferenceHeight = ((desksHeight - (maxYcoordinate + deskSize)) - (minYCoordinate))/2
        }
        else{
            shiftDifferenceHeight = (minYCoordinate * -1)
        }
        
        
        contentview.setFrameSize(NSMakeSize(desksWidth, desksHeight))
        contentScrollView.documentView?.setFrameSize(NSMakeSize(desksWidth, desksHeight))
        for view in contentScrollView.documentView!.subviews as! [Student]{
            
            //    var   startingPanLocation = view.frame.origin
            
            view.frame.origin.x  += shiftDifferenceWidth
            view.frame.origin.y += shiftDifferenceHeight
            
        }
        
    }
    
    func loadTotalAttendance(){
        
        for student in self.contentScrollView.documentView!.subviews as! [Student]{
            student.presentTotal = 0
            student.attendanceTotal = 0
            student.absentTotal = 0
            student.lateTotal = 0
        }
        
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
            var total : [AnyObject]  = []
            dispatch_async(dispatch_get_main_queue()) {
                
                let predicate = NSPredicate(format: "ClassName = %@",self.classData.name)
                
                let query = PFQuery(className:"AttendanceInfo",predicate: predicate)
                query.findObjectsInBackgroundWithBlock {
                    (objects: [AnyObject]?, error: NSError?) -> Void in
                    if error == nil {
                        if let objects = objects  {
                            for object in objects {
                                
                                
                                total.append(object["attendanceDictionary"]!!)
                                
                                
                            }
                            // Swift.print(total)
                            //set the values of total attendance
                            
                            for details in total {
                                
                                var markedValue : String = ""
                                for view in self.contentScrollView.documentView!.subviews as! [Student]{
                                    let lengthUpToSNumber = (view).sid.rangeOfString("@")
                                    if(details.valueForKey(view.sid.substringToIndex(lengthUpToSNumber.location)) != nil){
                                        
                                        
                                    markedValue  = ((details[view.sid.substringToIndex(lengthUpToSNumber.location)]!)?.description!)!
                                         }
                                    
                                    
                                    switch (markedValue){
                                    case "0" :
                                        view.presentTotal += 1
                                        view.attendanceTotal += 1
                                        
                                    case "1":
                                        view.absentTotal += 1
                                        view.attendanceTotal += 1
                                        
                                    case "2":
                                        view.lateTotal += 1
                                        view.attendanceTotal += 1
                                        
                                    default : Swift.print("")
                                    }
                                }
                                
                            }
                            
                            
                            
                            
                            
                        }
                        self.loadAttendance()
                      
                        
                    } else {
                        
                        print("Error: \(error!) \(error!.userInfo)")
                    }
                }
            }
        }// end of dispatch_async
        
        
        
    }
    
    
    //
    func loadAttendance(){
        
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
            var attendance : [String:NSString] = [:]
            dispatch_async(dispatch_get_main_queue()) {
                //                let predicate = NSPredicate(format: "dateString = %@ AND ClassName = %@",self.formatDate(self.datePickerDate.dateValue),self.classData.name)
                let predicate = NSPredicate(format: "ClassName = %@",self.classData.name)
                
                let query = PFQuery(className:"AttendanceInfo",predicate: predicate)
                query.findObjectsInBackgroundWithBlock {
                    (objects: [AnyObject]?, error: NSError?) -> Void in
                    if error == nil {
                        if let objects = objects  {
                            for object in objects {
                                
                                
                                if (object["dateString"] as! NSString) == self.formatDate(self.datePickerDate.dateValue){
                                    attendance = object["attendanceDictionary"] as! Dictionary
                                }
                                
                            }
                            // Swift.print(total)
                            //set the values of total attendance
                            
                            
                            
                            
                            if(attendance.count > 0 ){
                                
                                for view in self.contentScrollView.documentView!.subviews as! [Student]{
                                    let lengthUpToSNumber = (view).sid.rangeOfString("@")
                                    let value = attendance[view.sid.substringToIndex(lengthUpToSNumber.location)]?.integerValue
                                    if (value != nil){
                                    view.radio.selectCellAtRow(value!, column: 0)
                                    }
                                    
                                    
                                }
                                Swift.print("i am recoreed")
                                self.stausLabel.stringValue = "Recorded"
                            }
                            else{
                                for view in self.contentScrollView.documentView!.subviews as! [Student]{
                                    view.radio.selectCellAtRow(0, column: 0)
                                    
                                }
                                self.stausLabel.stringValue = "Not Recorded"
                               
                            }
                            
                        }
                        
                    } else {
                        
                        print("Error: \(error!) \(error!.userInfo)")
                    }
                }
            }
        }// end of dispatch_async
        
        
        
        
        
    }
    
    
    
    /****************************************************************************
    Function to initiate the third split view when user selects a student.
    *************************************************************************** */
    override func mouseDown(theEvent: NSEvent) {
        
        unhighlightOtherStudents(self)
        (self.mainSplitView.splitViewItems[2] ).collapsed = true
        
        
        
        self.startPoint = theEvent.locationInWindow
        self.endPoint = theEvent.locationInWindow
        self.startPoint.x -= 312
        self.startPoint.y -= 34
        
        shapeLayer = CAShapeLayer(layer: documentLayer)
        shapeLayer.lineWidth = 1.0
        shapeLayer.fillColor = CGColorGetConstantColor(kCGColorClear)
        shapeLayer.strokeColor = CGColorGetConstantColor(kCGColorBlack)
        documentLayer.addSublayer(shapeLayer)
        
        
    }
    
    /****************************************************************************
    Function to draw a red border around the selected student and save the notes of the student
    *************************************************************************** */
    func unhighlightOtherStudents(nsNotification:AnyObject){
        let studentVC : studentPopUpDetailVC = (self.mainSplitView.splitViewItems[2]).viewController as! studentPopUpDetailVC
        (self.mainSplitView.splitViewItems[2]).collapsed = true
        for view in contentScrollView.documentView!.subviews as! [Student]{
            (view).layer!.backgroundColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).CGColor
            if self.selectedStudent != nil{
                self.selectedStudent.notes = studentVC.notesTV.string!
            }
        }
    }
    
    
    /****************************************************************************
    Function to add new desk when user clicks '+' button.
    *************************************************************************** */
    @IBAction func addNewStudent(sender: AnyObject) {
        let frame = NSMakeRect(newDeskAddPosition_x  , newDeskAddPosition_y, deskSize, deskSize)
        let newStudent = Student(frame: frame , newCell: true, name: "new", sid : "noSid")
        NSAnimationContext.runAnimationGroup({(context)-> Void in
            
            context.duration = 4.0
            let newLabel = newStudent.subviews[2] as! NSImageView
            newLabel.animator().alphaValue = 0
            
            }, completionHandler: { () -> Void in
                
        })
        newStudent.sid = "noSid"
        newStudent.notes = "empty"
        newStudent.surname = "surname"
        newStudent.mainSplitVCDelegate = self.mainSplitView
        newStudent.classDisplayVCDelegate = self
        contentScrollView.documentView?.addSubview(newStudent)
        
        
    }
    
    /****************************************************************************
    Function to create click gesture recoginiser for deleting desks
    *************************************************************************** */
    
    func createGesture()-> NSClickGestureRecognizer{
        let clickgesture = NSClickGestureRecognizer()
        clickgesture.target = self
        clickgesture.numberOfClicksRequired = 1
        clickgesture.action = Selector("deleteDesk:")
        
        return clickgesture
    }
    
    /****************************************************************************
    Function to delete desks from the room.
    *************************************************************************** */
    @IBAction func deleteDesks(sender: AnyObject) {
        if sender.state == 1{
            _ = NSClickGestureRecognizer()
            for students in contentScrollView.subviews[0].subviews[0].subviews{
                let image = NSImageView()
                image.image = NSImage(named: "remove-151678_1280.png")
                image.addGestureRecognizer(createGesture())
                image.setFrameSize(NSMakeSize(20, 20))
                image.setFrameOrigin(NSMakePoint(60, 60))
                
                students.addSubview(image)
            }
            saveBTN.enabled = false
            addBTN.enabled = false
        }
        else{
            for students in contentScrollView.subviews[0].subviews[0].subviews{
                
                //removing the crossmark image from the desk
                for imageView in students.subviews{
                    if imageView .isKindOfClass(NSImageView){
                        if (imageView as! NSImageView).image == NSImage(named: "remove-151678_1280.png"){
                            imageView.removeFromSuperview()
                        }
                    }
                }
                //removing the gesture from the desk
                for gesture in students.gestureRecognizers{
                    if gesture.isKindOfClass(NSClickGestureRecognizer){
                        students.removeGestureRecognizer(gesture as! NSClickGestureRecognizer)
                    }
                }
                
            }
         saveBTN.enabled = true
            addBTN.enabled = true
        }
        
    }
    
    /***************************************************************************
    Function to delete desks from the room when a specific desk is clicked
    *************************************************************************** */
    func deleteDesk(ges : NSGestureRecognizer){
        let image = ges.view
        image!.superview!.removeFromSuperview()
    }
    
    /****************************************************************************
    Saves the entire data into Parse.
    *************************************************************************** */
    @IBAction func saveStudents(sender: AnyObject) {
        
        NSNotificationCenter.defaultCenter().postNotificationName("unhighlightOtherStudents", object: self)
        var deskXaxis : [CGFloat] = []
        var deskYaxis: [CGFloat] = []
        var studentNames : [NSString] = []
        var sidArr: [NSString] = []
        var notesOfStudents :[NSString] = []
        var attendanceDict :[NSString:NSString] = [:]
        for student in contentScrollView.subviews[0].subviews[0].subviews{
            deskXaxis.append(student.frame.origin.x)
            deskYaxis.append(student.frame.origin.y)
            studentNames.append(((student) as! Student).name)
            notesOfStudents.append((student as! Student).notes)
            sidArr.append((student as! Student).sid)
            let lengthUpToSNumber = (student as! Student).sid.rangeOfString("@")
            attendanceDict[(student as! Student).sid.substringToIndex(lengthUpToSNumber.location)] = (student as! Student).radio.selectedRow.description
            
        }
        classData.deskXaxis = deskXaxis
        classData.deskYaxis = deskYaxis
        classData.namesArray = studentNames
        classData.notesArray = notesOfStudents
        classData.sidArray = sidArr
        
        // saving room information on parse
        
        let predicate = NSPredicate(format: "name = '\(classData.name)'")
        let query = PFQuery(className:"ClassInfo",predicate: predicate)
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil {
                if let objects = objects  {
                    _ = objects.count
                    for classRoom in objects as! [PFObject] {
                        classRoom["deskXaxis"] = deskXaxis
                        classRoom["deskYaxis"] = deskYaxis
                        classRoom["studentNames"] = studentNames
                        classRoom["notes"] = notesOfStudents
                        classRoom["sid"] = sidArr
                        classRoom.saveInBackground()
                    }
                    for i in 0..<self.mainSplitView.classDetailsArray.count{
                        if self.mainSplitView.classDetailsArray[i].name == self.classData.name
                        {
                            self.mainSplitView.classDetailsArray[i] = self.classData
                            
                        }
                    }
                }
                else {
                    print(error)
                }
                
            }
            
            
        }
        let attendancePredicate = NSPredicate(format: "dateString = %@ AND ClassName = %@",formatDate(self.datePickerDate.dateValue),self.classData.name)
        let attendanceQuery = PFQuery(className: "AttendanceInfo",predicate: attendancePredicate)
        attendanceQuery.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil {
                if let objects = objects  {
                    _ = objects.count
                    if (objects.count > 0){
                        ( objects[0] as! PFObject)["attendanceDictionary"] = attendanceDict
                        ( objects[0] as! PFObject).saveInBackground()
                        self.stausLabel.stringValue = "Recorded"
                        self.loadTotalAttendance()
                    }
                    else{
                        self.recordAttendance(attendanceDict)
                        self.loadTotalAttendance()
                    }
                    
                    
                    
                }
                else {
                    print(error)
                }
                
            }
            
            
        }
        
        
        
        
        
    }
    
    
    func recordAttendance(attendanData : [NSString:NSString]){
        
        
        let attendance = PFObject(className:"AttendanceInfo")
        attendance["dateString"] = formatDate( datePickerDate.dateValue)
        attendance["attendanceDictionary"] = attendanData
        attendance["ClassName"] = classData.name
        attendance.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success) {
                attendance.saveInBackground()
                self.stausLabel.stringValue = "Recorded"
                // MasterVC roomTV is updated using loadRoomDetails and reloaded
                
            } else {
                print("not saved in parse")
                
                
            }
        }
        
        
        
    }
    
    
    func formatDate (date : NSDate ) -> String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        
        return dateFormatter.stringFromDate(date)
    }
    
    
    //Handling mouse events
    

    
    override func mouseDragged(theEvent: NSEvent) {
        self.endPoint = theEvent.locationInWindow
        self.endPoint.x -= 312
        self.endPoint.y -= 34
        
        let path : CGMutablePathRef = CGPathCreateMutable()
        CGPathMoveToPoint(path, nil, self.startPoint.x, self.startPoint.y)
        CGPathAddLineToPoint(path, nil, self.startPoint.x, self.endPoint.y)
        CGPathAddLineToPoint(path, nil, self.endPoint.x, self.endPoint.y)
        CGPathAddLineToPoint(path, nil, self.endPoint.x, self.startPoint.y)
        CGPathAddLineToPoint(path, nil, self.startPoint.x, self.startPoint.y)
    self.shapeLayer.path = path
        
        
        
    }
    override func mouseUp(theEvent: NSEvent) {
        if shapeLayer != nil{
        self.shapeLayer.removeFromSuperlayer()
        selectStudents()
        }
       
    }
    
    func selectStudents(){
        var minX: CGFloat = 0
        var minY : CGFloat = 0
        var maxX : CGFloat = 0
        var maxY : CGFloat = 0
        
        if self.startPoint.x < self.endPoint.x{
            minX = startPoint.x
            maxX = endPoint.x
        }
        else{
            maxX = startPoint.x
            minX = endPoint.x

        }
        
        if self.startPoint.y < self.endPoint.y{
            minY = startPoint.y
            maxY = endPoint.y
        }
        else{
            maxY = startPoint.y
            minY = endPoint.y

        }
        
       for student in contentScrollView.documentView!.subviews as! [Student]{
        
        if((student.frame.origin.x + 30) > minX && (student.frame.origin.x + 30) < maxX){
            if ((student.frame.origin.y + 30 ) > minY && (student.frame.origin.y + 30) < maxY){
              student.layer?.backgroundColor = NSColor.redColor().CGColor
                student.becomeFirstResponder()
                
            }
        }
        
        }//end of for loop
        
        
        
        
        
    }
    
    override func keyDown(theEvent: NSEvent) {
        Swift.print("hehe")
    }
    
    //---------------------------------------------ALL CODE FOR GROUPS IMPLEMENTATION---------------------------------
    
    //function to load groups
//    
//    func loadGroupsContainer(){
//        
//        
//        
//            
//            let contentview = NSView()
//            contentview.setFrameOrigin(NSMakePoint(0, 0))
//            
//            (self.mainSplitView.splitViewItems[2] ).collapsed = true
//            
//        
//        // no of groups
//        
//        let noOfGroups = 3
//        
//        //no of desks in a row
//            let noOfDeskainGroup = 2
//        //no of desks in a column
//    
//        
//        //calculating group width and height
//        
//        let groupWidth = CGFloat(noOfDeskainGroup) * (deskSize + CGFloat(10))
//        let groupHeight = CGFloat(noOfDeskainGroup) * (deskSize + CGFloat(10))
//        
//        var totalNoOfColums = 0
//        var totalNoOfRows = 0
//        
//        //calculate totalNooFRows and Colums
//        if self.contentScrollView.contentView.frame.width > groupWidth * CGFloat(noOfGroups){
//             totalNoOfColums = noOfGroups
//             totalNoOfRows = 1
//        }
//        else{
//            let capacity = Int(self.contentScrollView.contentView.frame.width / groupWidth)
//             totalNoOfColums = capacity
//             totalNoOfRows = 0
//            var required = noOfGroups
//            while(required>0){
//                required -= capacity
//                totalNoOfRows++
//            }
//        }
//        
//        
//        
//        //calcuate Total width and height
//        
//        var mainStaringX = 0
//        var mainStartingY = 0
//        var groupStaringX = mainStaringX
//        var groupStaringY = mainStartingY
//        
//        var totalWidth = groupWidth * CGFloat(totalNoOfColums)
//        var totalHeight = groupHeight * CGFloat(totalNoOfRows)
//        
//        contentview.setFrameSize(NSMakeSize(12 * deskSize + 100.0, 4 * deskSize + 100.0))
//        var count = 0
//        Swift.print("total no of column is \(totalNoOfColums)")
//        for mainRow in 0..<totalNoOfRows{
//                groupStaringX = mainStaringX
//            for mainCol in 0..<totalNoOfColums{
//                groupStaringY = mainStartingY + Int(groupHeight) - 30
//                
//                //code for group
//                for groupRow in 0..<2{
//                    for groupCol in 0..<2{
//                        
//        let frame =  NSMakeRect( CGFloat(groupStaringX + groupCol * Int(deskSize) + (Int(groupWidth) * mainCol) ), CGFloat(Int( groupStaringY) + groupRow * Int(deskSize) ) , deskSize, deskSize)
//                        
//                        student = Student(frame: frame, newCell :false,name : self.classData.namesArray[count],sid: classData.sidArray[count])
//                                        //student.layer!.backgroundColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).CGColor
//                                        student.layer?.backgroundColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).CGColor
//                                        student.notes = classData.notesArray[count++]
//                                        student.classDisplayVCDelegate = self
//                                        student.mainSplitVCDelegate = mainSplitView
//                                        contentview.addSubview(student)
//                        
//                    }
//                }
//                
//            }
//            
//            
//        }
//        
//        
//            
//        contentScrollView.documentView = contentview
//        documentLayer  = (contentScrollView.documentView! as! NSView).layer!
//        documentLayer.backgroundColor = CGColorCreateGenericRGB(0.8, 8, 1, 1)
//        
//        //Swift.print(contentview.subviews)
//
//        
//        
//        
//            
//            
//            
////            // The container width and height of the desks if they were seen as placed in rectangle
////            var desksWidth = (maxXCoordinate - minXCoordinate) + deskSize
////            var desksHeight = maxYcoordinate - minYCoordinate + deskSize
////            
////            shiftDifferenceWidth  = 0
////            shiftDifferenceHeight  = 0
////            
////            // calculating the deskwidth and shiftWidthDifference
////            if self.contentScrollView.contentView.frame.width > desksWidth{
////                desksWidth = self.contentScrollView.contentView.frame.width
////                
////                shiftDifferenceWidth = ((desksWidth - (maxXCoordinate + deskSize)) - (minXCoordinate))/2
////                
////                
////            }
////            else{
////                shiftDifferenceWidth =  (minXCoordinate * -1)
////            }
////            
////            
////            // calculating the deskheight and shiftHeightDifference
////            if (self.contentScrollView.contentView.frame.height ) > desksHeight{
////                desksHeight = self.contentScrollView.contentView.frame.height
////                shiftDifferenceHeight = ((desksHeight - (maxYcoordinate + deskSize)) - (minYCoordinate))/2
////            }
////            else{
////                shiftDifferenceHeight = (minYCoordinate * -1)
////            }
////            
////            
////            contentview.setFrameSize(NSMakeSize(desksWidth, desksHeight))
////            
////            
////            for i in 0..<(sender as! Class).deskXaxis.count{
////                
////                let frame = NSMakeRect(classData.deskXaxis[i] + shiftDifferenceWidth , classData.deskYaxis[i] + shiftDifferenceHeight, deskSize, deskSize)
////                
////                
////                name = (sender as! Class).namesArray[i]
////                sid = (sender as! Class).sidArray[i]
////                notes = (sender as! Class).notesArray[i]
////                
////                
////                student = Student(frame: frame, newCell :false,name : name,sid: sid)
////                //student.layer!.backgroundColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).CGColor
////                student.layer?.backgroundColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).CGColor
////                student.notes = notes
////                student.classDisplayVCDelegate = self
////                student.mainSplitVCDelegate = mainSplitView
////                contentview.addSubview(student)
////                //                self.selectedStudent = student
////            }
////            contentScrollView.documentView = contentview
////            documentLayer  = (contentScrollView.documentView! as! NSView).layer!
////            documentLayer.backgroundColor = CGColorCreateGenericRGB(0.8, 8, 1, 1)
////        }
////        //  loadAttendance()
////        datePickerDate.dateValue = NSDate()
////        Swift.print(NSDate())
////        loadTotalAttendance()
// 
//    }
    

    override func prepareForSegue(segue: NSStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addGroupSegue"{
            
         let addGroups =   segue.destinationController as! AddRandomGroups
         addGroups.classData = self.classData
            addGroups.classDisplayVCDeligate = self
            
            
            
    }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "addGroupSegue" && self.classOrGroupSegment.selectedSegment == 0{
            self.loadContainer(self.classData)
            return false
        }
        
        
        return true
    }
    
    
    
}



class hahah{
    
}
