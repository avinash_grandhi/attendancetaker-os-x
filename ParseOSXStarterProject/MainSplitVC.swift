//
//  MainSplitVC.swift
//  AttendanceTakerOSX
//
//  Created by admin on 7/18/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import Cocoa
import ParseOSX

/****************************************************************************
This class controls all the three split view controllers based on the user's selections.
*************************************************************************** */
class MainSplitVC: NSSplitViewController , NSTableViewDataSource, NSTableViewDelegate{
    
    /* ****************************************************************************
    masterView
    \
    \
    |---------|---------------------|
    |-classTV-|                     |
    |---------|                     |
    |---------|                     |
    |---------|      DetailVC       |
    |---------|                     |
    |      - +|                     |
    |---------|                     |
    |-roomTV--|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |      - +| <-------------------|-------delRoomButton and addRoomButton
    |---------|---------------------|
    
    
    
    ****************************************************************************
    Variables for the MainSplitVC
    *************************************************************************** */
    
    var masterView:MasterVC{
        let masterItem = self.splitViewItems[0] 
        return masterItem.viewController as! MasterVC
    }
    
    var detailVC : DetailVC{
        let detailItem = self.splitViewItems[1] 
        return detailItem.viewController as! DetailVC
    }
    
    var studentDetailPopUpVC : NSViewController{
        let popUpItem = self.splitViewItems[2] 
        return popUpItem.viewController
    }
    var delRoomButton : NSButton{
        return masterView.deleteRoomButton as NSButton
    }
    
    var delClassButton : NSButton{
        return masterView.deleteClassButton as NSButton
    }
    
    var roomsDetailsArray : [Room] = []
    var classDetailsArray : [Class] = []
    
    /****************************************************************************
    Delegating roomTV and classTV to the MainSplitVC
    Calling the loadRoomDetails to load values from parse
    *************************************************************************** */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delegating masterview tables
        masterView.roomTV.setDataSource(self)
        masterView.roomTV.setDelegate(self)
        
        masterView.classTV.setDataSource(self)
        masterView.classTV.setDelegate(self)
        
        //delegating self for masterview
        masterView.splitView = self
        
        //delegating delete button for room
        delRoomButton.target = self
        delRoomButton.action = Selector("deleteSelectedRoom:")
        
        //delegating delete button for class
        delClassButton.target = self
        delClassButton.action = Selector("deleteSelectedClass:")
        
        //assigning self to the appDelegate mainSplitVC variable
        (NSApplication.sharedApplication().delegate as! AppDelegate).mainSplitVC = self
        loadRoamDetails()
        loadClassRooms()
    }
    
    /****************************************************************************
    Updating the window title when view appears
    *************************************************************************** */
    
    override func viewDidAppear() {
        super.viewDidAppear()
        self.view.window?.title = "Attendance Taker"
        let statusmenu = (NSApplication.sharedApplication().delegate as! AppDelegate).statusMenu
        statusmenu.title = "Attendance Taker OSX"
        statusmenu.itemAtIndex(8)?.enabled = true
        statusmenu.itemAtIndex(8)?.attributedTitle = NSAttributedString(string: "Delete Room")
        statusmenu.itemAtIndex(8)?.action = Selector("deleteSelectedRoom:")
        statusmenu.itemAtIndex(7)?.enabled = true
        statusmenu.itemAtIndex(7)?.attributedTitle = NSAttributedString(string: "Add Room")
        statusmenu.itemAtIndex(7)?.action = Selector("addRoom")
        (NSApplication.sharedApplication().delegate as! AppDelegate).statusMenu = statusmenu
        (self.splitViewItems[2] ).collapsed = true
    }
    
    /****************************************************************************
    Instantiating another thread to get room data from parse
    Load it in roomDetails array
    Reload data in roomTV after loading the data
    *************************************************************************** */
    
    func loadRoamDetails(){
        var roomData :Room!
        self.roomsDetailsArray = []
        // another thread for getting details from parse
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
            dispatch_async(dispatch_get_main_queue()) {
                let query = PFQuery(className:"DeskInfo")
                query.findObjectsInBackgroundWithBlock {
                    (objects: [AnyObject]?, error: NSError?) -> Void in
                    if error == nil {
                        if let objects = objects  {
                            let totalRows = objects.count
                            for object in objects {
                                roomData = Room(number: "", rows: 0, cols: 0)
                                roomData.noOfRows = object["rows"] as! Int
                                roomData.noOfCols = object["cols"] as! Int
                                roomData.roomNumber = object["roomNumber"] as! NSString
                                roomData.deskXaxis = object["deskXaxis"] as! [CGFloat]
                                roomData.deskYaxis = object["deskYaxis"] as! [CGFloat]
                                //loading data in roomDetails array
                                self.roomsDetailsArray.append(roomData)
                                if self.roomsDetailsArray.count == totalRows{
                                    // reloading data in roomTV after loading the data
                                    self.masterView.roomTV.reloadData()
                                }
                            }
                        }
                        
                    } else {
                        
                        print("Error: \(error!) \(error!.userInfo)")
                    }
                }
            }
        }// end of dispatch_async
    }
    
    func loadClassRooms(){
        var classObject : Class!
        self.classDetailsArray = []
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
            dispatch_async(dispatch_get_main_queue()) {
                let query = PFQuery(className:"ClassInfo")
                query.findObjectsInBackgroundWithBlock {
                    (objects: [AnyObject]?, error: NSError?) -> Void in
                    if error == nil {
                        if let objects = objects  {
                            let totalRows = objects.count
                            for object in objects {
                                classObject = Class(name: "", namesArray: [], surnameArray : [], deskXaxis: [], deskYaxis: [])
                                classObject.name = object["name"] as! NSString
                                classObject.namesArray = object["studentNames"] as! [NSString]!
                                classObject.deskXaxis = object["deskXaxis"] as! [CGFloat]
                                classObject.deskYaxis = object["deskYaxis"] as! [CGFloat]
                                classObject.notesArray = object["notes"] as! [NSString]
                                classObject.sidArray = object["sid"] as! [NSString]
                                classObject.surnameArray = object["studentSurnames"] as! [NSString]!
                                self.classDetailsArray.append(classObject)
                                
                                //loading data in roomDetails array
                                //    self.roomsDetailsArray.append(roomData)
                                if self.classDetailsArray.count == totalRows{
                                    // reloading data in roomTV after loading the data
                                    self.masterView.classTV.reloadData()
                                }
                                
                            }
                            
                            
                        }
                        
                    } else {
                        
                        print("Error: \(error!) \(error!.userInfo)")
                    }
                }
            }
        }// end of dispatch_async
    }
    
    /****************************************************************************
    Adding the room
    *************************************************************************** */
    func addRoom(){
        self.presentViewControllerAsSheet(self.storyboard?.instantiateControllerWithIdentifier("addRoomVC") as! AddRoomVC)
    }
    
    /****************************************************************************
    Deleting the specific room in parse
    Updating the roomdetailsarray
    Reloading the roomTV tableView
    Emptying the detailVC
    *************************************************************************** */
    
    func deleteSelectedRoom(object: AnyObject){
        
        //deleting the specific room in parse
        if self.masterView.roomTV.selectedRow >= 0{
            let predicate = NSPredicate(format: "roomNumber = '\(self.roomsDetailsArray[self.masterView.roomTV.selectedRow].roomNumber)'")
            let query = PFQuery(className:"DeskInfo",predicate: predicate)
            query.findObjectsInBackgroundWithBlock {
                (objects: [AnyObject]?, error: NSError?) -> Void in
                
                if error == nil {
                    if let objects = objects  {
                        _ = objects.count
                        for object in objects {
                            (object as! PFObject).deleteInBackground()
                            
                        }
                        
                    }
                }
                
            }//end of query
            
            //updating the roomdetailsarray
            self.roomsDetailsArray.removeAtIndex(self.masterView.roomTV.selectedRow)
            //reloading the roomTV tableView
            self.masterView.roomTV.reloadData()
            //to empty the detailVC
            (self.detailVC.tabView.tabViewItemAtIndex(0).viewController as!  RoomDisplayVC).contentScrollView.documentView = nil
        }
        else{
            let alert = NSAlert()
            alert.informativeText = "You should select a room before deleting"
            alert.messageText = "Message"
            alert.addButtonWithTitle("Ok")
            alert.runModal()
            
        }
    }
    
    /****************************************************************************
    Deleting the specific class in parse
    Updating the classDetailsArray
    Reloading the classTV tableView
    Emptying the detailVC
    *************************************************************************** */
    
    func deleteSelectedClass(object: AnyObject){
        //deleting the specific room in parse
        if self.masterView.classTV.selectedRow >= 0{
            let predicate = NSPredicate(format: "name = '\(self.classDetailsArray[self.masterView.classTV.selectedRow].name)'")
            let query = PFQuery(className:"ClassInfo",predicate: predicate)
            query.findObjectsInBackgroundWithBlock {
                (objects: [AnyObject]?, error: NSError?) -> Void in
                
                if error == nil {
                    if let objects = objects  {
                      //  var totalRows = objects.count
                        for object in objects {
                            (object as! PFObject).deleteInBackground()
                            
                        }
                        
                    }
                }
                
            }//end of query
            
            //updating the roomdetailsarray
            self.classDetailsArray.removeAtIndex(self.masterView.classTV.selectedRow)
            //reloading the roomTV tableView
            self.masterView.classTV.reloadData()
            //to empty the detailVC
            (self.detailVC.tabView.tabViewItemAtIndex(1).viewController as!  ClassDisplayVC).contentScrollView.documentView = nil
        }//end of if
        else{
            let alert = NSAlert()
            alert.informativeText = "You should select a class before deleting"
            alert.messageText = "Message"
            alert.addButtonWithTitle("Ok")
            alert.runModal()
        }
    }
    
    /****************************************************************************
    Gets the room details and update the roomDetailsArray
    Save it in parse
    *************************************************************************** */
    func saveClassRoom( roomData : Room){
        if masterView.roomTV.selectedRow >= 0{
            let deskXaxis : [CGFloat] = roomData.deskXaxis
            let deskYaxis : [CGFloat] = roomData.deskYaxis
            
            // update the roomDetailsArray
            for room in roomsDetailsArray{
                
                if room.roomNumber == roomData.roomNumber{
                    room.deskXaxis = roomData.deskXaxis
                    room.deskYaxis = roomData.deskYaxis
                }
            }
            
            // saving room information on parse
            let predicate = NSPredicate(format: "roomNumber = '\(roomData.roomNumber)'")
            let query = PFQuery(className:"DeskInfo",predicate: predicate)
            query.findObjectsInBackgroundWithBlock {
                (objects: [AnyObject]?, error: NSError?) -> Void in
                if error == nil {
                    if let objects = objects  {
                        let totalRows = objects.count
                        print("total rows is \(totalRows)")
                        for room  in objects as! [PFObject] {
                            room["deskXaxis"] = deskXaxis
                            room["deskYaxis"] = deskYaxis
                            room.saveInBackground()
                            print(deskXaxis.count)
                            print(deskYaxis.count)
                        }
                    }
                    else {
                        print(error)
                    }
                }
                
            }
        }
        
    }
    
    /****************************************************************************
    Returns the number for roomTV based on roomDetailsArray size
    Returns the static count of 2 for classTV
    *************************************************************************** */
    
    func numberOfRowsInTableView(aTableView: NSTableView) -> Int {
        
        let currFrame  : NSRect =  masterView.classTV.headerView!.frame
        masterView.classTV.headerView?.frame = CGRectMake(currFrame.origin.x,currFrame.origin.y, currFrame.size.width,currFrame.size.height+5);
        masterView.roomTV.headerView?.frame = CGRectMake(currFrame.origin.x,currFrame.origin.y, currFrame.size.width,currFrame.size.height+5);
        if aTableView == masterView.classTV{
            return classDetailsArray.count
        }
        else{
            return roomsDetailsArray.count
        }
    }
    
    /****************************************************************************
    Returns the cell for roomTV based on content in roomDetailsArray
    Returns the cell for classTV based on row value
    *************************************************************************** */
    func tableView(tableView: NSTableView, viewForTableColumn tableColumn: NSTableColumn?, row: Int) -> NSView? {
        var cellView: NSTableCellView!
        if tableView == masterView.classTV{
            cellView = tableView.makeViewWithIdentifier(tableColumn!.identifier, owner: self) as! NSTableCellView
            cellView.textField?.stringValue = classDetailsArray[row].name.description
        }
        else{
            cellView = tableView.makeViewWithIdentifier(tableColumn!.identifier, owner: self) as! NSTableCellView
            cellView.textField?.stringValue = roomsDetailsArray[row].roomNumber.description
        }
        return cellView
    }
    
    
    /****************************************************************************
    Based on the tableView clicked, specific tab is selected in detailVC
    *************************************************************************** */
    
    func tableView(tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
        if tableView == masterView.classTV{
            //deselecting the selected row in roomTV f
            masterView.roomTV.deselectAll(nil)
            detailVC.selectedTabViewItemIndex = 1
            ( self.detailVC.tabViewItems[1].viewController as! ClassDisplayVC).mainSplitView = self
            ( self.detailVC.tabViewItems[1].viewController as! ClassDisplayVC).loadContainer(self.classDetailsArray[row])
            
            
        }
        else{
            //deselecting the selected row in classTV
            masterView.classTV.deselectAll(nil)
            
            // if roomTV is clicked RoomDisplayVC is displayed in DetailVC
            detailVC.selectedTabViewItemIndex = 0
            ( self.detailVC.tabViewItems[0].viewController as! RoomDisplayVC).mainSplitView = self
            ( self.detailVC.tabViewItems[0].viewController as! RoomDisplayVC).loadContainer(self.roomsDetailsArray[row])
            
        }
        return true
    }
    
}
