//
//  ClassTV.swift
//  AttendanceTakerOSX
//
//  Created by admin on 7/18/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import Cocoa

/* ****************************************************************************
masterVC
\
\
|---------|---------------------|
|-classTV-|                     |
|---------|                     |
|---------|                     |
|---------|                     |
|---------|                     |
|      - +|                     |
|---------|                     |
|---------|                     |
|---------|                     |
|---------|                     |
|---------|                     |
|      - +| <-------------------|----- addRoomButton and deleteRoomButton
|---------|---------------------|

****************************************************************************/


/****************************************************************************
This ViewController is initially loaded when the app is launched, with all the classes in a table view. *************************************************************************** */

class ClassTV: NSTableView {
    
}
