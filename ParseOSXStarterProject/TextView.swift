//
//  textView.swift
//
//
//  Created by admin on 7/5/15.
//
//

import Cocoa

/****************************************************************************
This class can be used to create NSTextView programmatically.
*************************************************************************** */

class TextView: NSTextField {
    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)
    }
}
