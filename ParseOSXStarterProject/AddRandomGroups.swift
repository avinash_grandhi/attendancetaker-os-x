//
//  AddRandomGroups.swift
//  AttendanceTakerOSX
//
//  Created by admin on 10/25/15.
//  Copyright © 2015 Parse. All rights reserved.
//

import Cocoa

class AddRandomGroups: NSViewController {
    
    
    
    
    
    @IBOutlet weak var groupSizeList: NSPopUpButton!
    
    @IBOutlet weak var noOfGroupsList: NSPopUpButton!
    
    var classDisplayVCDeligate : ClassDisplayVC!
    var classData : Class!
    var groupSize : Int!
    var numberOFGroups : Int!
    var totalStudents : Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        totalStudents = self.classData.namesArray.count
        
        groupSizeList.removeAllItems()
        noOfGroupsList.removeAllItems()
        groupSizeList.addItemWithTitle("? in group")
        noOfGroupsList.addItemWithTitle("? Groups")
        for i in 1...totalStudents{
            groupSizeList.addItemWithTitle("\(i) in group")
            noOfGroupsList.addItemWithTitle("\(i) Groups")
        }
        
        
    }
    
    
    
    
    @IBAction func updateMaxGroups(sender: NSPopUpButton) {
        
        //Swift.print(sender.selectedItem?.title)
        
          groupSizeList.selectItemAtIndex(0)

        
    }
    

    @IBAction func updateMaxMembersInGroup(sender: NSPopUpButton) {
        
       // Swift.print(sender.selectedItem?.title)
        
        noOfGroupsList.selectItemAtIndex(0)
            }
    
    
    @IBAction func createGroups(sender: NSButton) {
        
        if noOfGroupsList.selectedItem?.title == "? Groups" && groupSizeList.selectedItem?.title == "? in group"{
            displayAlert("Please select groups or member size to be grouped")
        }
        else{
        
            Swift.print(self.groupSizeList.title)
            Swift.print(self.noOfGroupsList.title)
            
            if noOfGroupsList.selectedItem?.title != "? Groups"{
                makeGroupsBasedOnNumberOfGroups()
            }
            else{
                makeGroupsBasedOnGroupSize()
            }

        }
        
        
        
    }
    
    
    @IBAction func cancelGroupMaker(sender: NSButton) {
        self.dismissViewController(self)
    }
    
    
    /****************************************************************************
    Handle popups when any information is not provided as needed.
    *************************************************************************** */
    func displayAlert(message : NSString){
        let alert = NSAlert()
        alert.informativeText = message.description
        alert.messageText = "Message"
        alert.addButtonWithTitle("OK")
        alert.runModal()
    }
    
    
    func makeGroupsBasedOnGroupSize(){
        
        //groupSize = ((self.groupSizeList.title as NSString).string)
        groupSize   = ((self.groupSizeList.title as NSString).componentsSeparatedByString(" ")[0] as NSString).integerValue
              
        numberOFGroups = totalStudents % groupSize == 0 ? (totalStudents / groupSize) : (totalStudents / groupSize + 1)
       
        Swift.print(totalStudents)
        Swift.print("number of goups \(numberOFGroups)")
        Swift.print("size of group \(groupSize)")
        Swift.print()
        
        let random = RandomGenerator()
        random.classDisplayVCDelegate = self.classDisplayVCDeligate
        random.noOfGroups = numberOFGroups
        random.noOfDeskainGroup = groupSize
        random.loadGroupsContainer()
        
        self.dismissViewController(self)


    }
    
    func makeGroupsBasedOnNumberOfGroups(){
        numberOFGroups = ((self.noOfGroupsList.title as NSString).substringToIndex(1) as NSString).integerValue
        
        groupSize = totalStudents % numberOFGroups == 0 ? (totalStudents / numberOFGroups) : (totalStudents / numberOFGroups + 1)
        
        Swift.print(totalStudents)
        Swift.print("number of goups \(numberOFGroups)")
        Swift.print("size of group \(groupSize)")
        Swift.print()
        let random = RandomGenerator()
        random.classDisplayVCDelegate = self.classDisplayVCDeligate
        random.noOfGroups = numberOFGroups
        random.noOfDeskainGroup = groupSize
        random.loadGroupsContainer()
        self.dismissViewController(self)


        
    }
    
    
}
