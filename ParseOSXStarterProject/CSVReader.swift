//
//  CSVReader.swift
//  AttendanceTakerOSX
//
//  Created by admin on 7/23/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import Foundation


/****************************************************************************
This class parses CSV files according to the specified format.
*************************************************************************** */

class CSVReader {
    var headers: [String] = [] //Stores headers of the file, probably first row
    var rows: [Dictionary<String, String>] = [] //Strores each row
    var columns = Dictionary<String, [String]>() //Every record consists of same number of values. Values of each header for every record is stored in columns.
    var delimiter = NSCharacterSet(charactersInString: ",") //Delimiter for CSV files is comma.
    
    /**************************************************************************
    //Used to create an instance of this class.
    *************************************************************************/
    
    init(contentsOfURL url: NSURL, delimiter: NSCharacterSet, encoding: UInt) throws {
        let csvString: String?
        do {
            csvString = try String(contentsOfURL: url, encoding: encoding)
        } catch _ {
            csvString = nil
        };
        if let csvStringToParse = csvString {
            self.delimiter = delimiter
            let newline = NSCharacterSet.newlineCharacterSet()
            var lines: [String] = []
            csvStringToParse.stringByTrimmingCharactersInSet(newline).enumerateLines { line, stop in lines.append(line) }
            self.headers = self.parseHeaders(fromLines: lines)
            self.rows = self.parseRows(fromLines: lines)
            self.columns = self.parseColumns(fromLines: lines)
        }
    }
    
    /**************************************************************************
    Supporting initializers. Calls the designated initializer.
    *************************************************************************/
    
    convenience init(contentsOfURL url: NSURL) throws {
        let comma = NSCharacterSet(charactersInString: ",")
        try self.init(contentsOfURL: url, delimiter: comma, encoding: NSUTF8StringEncoding)
    }
    
    
    /**************************************************************************
    Supporting initializers with different number of parameters shown above. Calls the designated initializer. ***********************************************************************/
    
    convenience init(contentsOfURL url: NSURL, encoding: UInt) throws {
        let comma = NSCharacterSet(charactersInString: ",")
        try self.init(contentsOfURL: url, delimiter: comma, encoding: encoding)
    }
    
    /**************************************************************************
    The first line of the file includes headers. This method parses headers and stores them in 'headers' array.
    *************************************************************************/
    
    func parseHeaders(fromLines lines: [String]) -> [String] {
        return lines[0].componentsSeparatedByCharactersInSet(self.delimiter)
    }
    
    /**************************************************************************
    This method parses each record(row) in the CSV file and stores in the rows.
    *************************************************************************/
    
    func parseRows(fromLines lines: [String]) -> [Dictionary<String, String>] {
        var rows: [Dictionary<String, String>] = []
        for (lineNumber, line) in lines.enumerate() {
            if lineNumber == 0 {
                continue
            }
            var row = Dictionary<String, String>()
            let values = line.componentsSeparatedByCharactersInSet(self.delimiter)
            for (index, header) in self.headers.enumerate() {
                if index < values.count {
                    row[header] = values[index]
                } else {
                    row[header] = ""
                }
            }
            rows.append(row)
        }
        return rows
    }
    
    /**************************************************************************
    Parses each record and stores the value of each header belonging to every record in the columns.
    *************************************************************************/
    
    func parseColumns(fromLines lines: [String]) -> Dictionary<String, [String]> {
        var columns = Dictionary<String, [String]>()
        for header in self.headers {
            let column = self.rows.map { row in row[header] != nil ? row[header]! : "" }
            columns[header] = column
        }
        return columns
    }
}
