//
//  roomDetails.swift
//  Sprint2
//
//  Created by admin on 7/5/15.
//  Copyright (c) 2015 admin. All rights reserved.
//

import Cocoa

/****************************************************************************
Room class has all the vairables which the specific room should have
*************************************************************************** */

class Room: NSObject {
    var roomNumber : NSString! //Each room has a name associated with it.
    var noOfRows : Int! //Number or rows in the room.
    var noOfCols : Int! //Number of columns in the room.
    var arrayOfDesks : [Desk]! //Room consists of (rows * columns) desks. So, it should be an array obviously.
    var deskXaxis : [CGFloat]! // Consists of each deks's x-coordinate value
    var deskYaxis : [CGFloat]! // Consists of each deks's y-coordinate value
    
    /****************************************************************************
    Used to create an instance of this class.
    *************************************************************************** */
    
    init(number : NSString, rows : Int, cols : Int) {
        self.roomNumber = number
        self.noOfCols = cols
        self.noOfRows = rows
    }
}
