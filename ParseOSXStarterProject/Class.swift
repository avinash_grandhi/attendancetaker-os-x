//
//  Class.swift
//  AttendanceTakerOSX
//
//  Created by admin on 7/23/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import Foundation

/****************************************************************************
Class has all the vairables which the specific class should have.
*************************************************************************** */
class Class: NSObject{
    
    var name : NSString! //Each class has a name associated with it.
    var deskXaxis : [CGFloat]! // Consists of each deks's x-coordinate value
    var deskYaxis : [CGFloat]! // Consists of each deks's y-coordinate value
    var namesArray : [NSString]! // Consists of student's name associated with that class to distinguish between each student.
    var sidArray : [NSString]!// consists of all the student sids
    var notesArray : [NSString]!//consists of all the student notes
    var surnameArray : [NSString]!
    
    /****************************************************************************
    Used to create an instance of this class.
    *************************************************************************** */
    
    init(name : NSString, namesArray : [NSString], surnameArray :[NSString], deskXaxis : [CGFloat], deskYaxis : [CGFloat]) {
        self.name = name
        self.namesArray = namesArray
        self.deskXaxis = deskXaxis
        self.deskYaxis = deskYaxis
        self.surnameArray = surnameArray
    }
    
}
