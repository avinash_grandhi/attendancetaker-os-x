//
//  MasterVC.swift
//  AttendanceTakerOSX
//
//  Created by admin on 7/18/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import Cocoa

/****************************************************************************
Handles two table views holding classes and rooms.
*************************************************************************** */
class MasterVC: NSViewController {
    
    
    /* ****************************************************************************
    masterVC
    \
    \
    |---------|---------------------|
    |-classTV-|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |      - +|                     |
    |---------|                     |
    |-roomTV--|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |      - +| <-------------------|----- addRoomButton and deleteRoomButton
    |---------|---------------------|
    
    ****************************************************************************
    Variables for the MasterVC
    *************************************************************************** */
    
    
    var splitView : MainSplitVC!
    
    @IBOutlet weak var classTV: ClassTV!
    @IBOutlet weak var roomTV: RoomTV!
    @IBOutlet weak var addRoomButton: NSButton!
    @IBOutlet weak var deleteRoomButton: NSButton!
    @IBOutlet weak var deleteClassButton: NSButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    /****************************************************************************
    Called when the addButton is clicked
    AddRoomVC is presented via segue id "ShowAddSegue"
    Initiating the masterController value of the destination view controller
    *************************************************************************** */
    
    
    override func prepareForSegue(segue: NSStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowAddSegue"
        {
            let destinationVC = segue.destinationController as? AddRoomVC
            //initiating the masterController value of the destination view controller
            destinationVC!.masterController = self
        }
    }
}
