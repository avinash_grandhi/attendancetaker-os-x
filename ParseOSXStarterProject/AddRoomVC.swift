
//
//  Created by admin on 7/13/15.
//  Copyright (c) 2015 admin. All rights reserved.
//

import Cocoa
import ParseOSX

/****************************************************************************
This ViewController is instantiated when the user clicks the Add room button (+) *************************************************************************** */
class AddRoomVC: NSViewController  , NSTextFieldDelegate , NSTextDelegate   {
    
    /* ****************************************************************************
                AddRoomVC
                    \
    |---------|----|-\--------------|---|
    |---------|    |   : columnTF   |   |
    |---------|    |   : rowsTF     |   |
    |---------|    |   : classNameTF|   |
    |---------|    |                |   |
    |---------|    | cancel    add  |   |
    |      - +|    |________________|   |
    |---------|                         |
    |---------|                         |
    |---------|                         |
    |---------|                         |
    |---------|                         |
    |      - +|                         |
    |---------|-------------------------|
    
    ****************************************************************************
    Variables for the AddRoomVC. MasterVC variable which is instantiated during prepare segue method in MasterVC
    *************************************************************************** */
   
    var masterController : MasterVC!
    @IBOutlet weak var columnsTF: NSTextField!
    @IBOutlet weak var rowsTF: NSTextField!
    @IBOutlet weak var classNameTF: NSTextField!
    
   /****************************************************************************
  Method invoked when text in a control such as a text field or form changes.
    *************************************************************************** */
    override func  controlTextDidChange(obj: NSNotification) {
        let formatter = NSNumberFormatter()
        if  (obj.object as! NSTextField).tag == 25 || (obj.object as! NSTextField).tag == 54 {
            if  formatter.numberFromString((obj.object as! NSTextField).stringValue) == nil{
                let alert = NSAlert()
                alert.informativeText = "Please enter only number."
                alert.messageText = "Message"
                alert.addButtonWithTitle("Ok")
                alert.runModal()
            }
        }
        
    }
    
    /****************************************************************************
   This method is invoked when the user stops editing text in a control such as a text field or form.
    *************************************************************************** */
    override func controlTextDidEndEditing(obj: NSNotification) {
        if  (obj.object as! NSTextField).tag == 16 {
            for room in   (NSApplication.sharedApplication().delegate as! AppDelegate).mainSplitVC.roomsDetailsArray{
                if room.roomNumber == (obj.object as! NSTextField).stringValue{
                    
                    (obj.object as! NSTextField).stringValue = ""
                    let alert = NSAlert()
                    alert.informativeText = "The room already exits. Please use the new room number."
                    alert.messageText = "Message"
                    alert.addButtonWithTitle("Ok")
                    alert.runModal()
                }
            }
        }
    }
    
    /****************************************************************************
    View will be dismissed when cancel button is clicked
    *************************************************************************** */
    
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewController(self)
    }

    /****************************************************************************
    Based on the rows and columns specific number of desk positions are generated
    New object is created for DeskInfo in parse and it is saved 
    MasterVC roomTV is updated using loadRoomDetails and reloaded
    AddRoomVC viewcontroller is dismissed
    *************************************************************************** */
    @IBAction func addFunction(sender: AnyObject) {
        let classRoomDataParse = PFObject(className:"DeskInfo")
        classRoomDataParse["rows"] = rowsTF.integerValue
        classRoomDataParse["cols"] = columnsTF.integerValue
        classRoomDataParse["roomNumber"] =  classNameTF.stringValue
        var deskXCordinatesArray : [CGFloat] = []
        var deskYCordinatesArray  : [CGFloat] = []
        
        // generate the coordinates for the benches
        for i in 0..<rowsTF.integerValue {
            for j in 0..<columnsTF.integerValue {
                deskXCordinatesArray.append(CGFloat(90 * j)+10)
                deskYCordinatesArray.append(CGFloat(90 * i) + 10)
            }
        }
        classRoomDataParse["deskXaxis"] = deskXCordinatesArray
        classRoomDataParse["deskYaxis"] = deskYCordinatesArray
        
        
        classRoomDataParse.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success) {
                classRoomDataParse.saveInBackground()
            } else {
                print("not saved in parse")
            }
        }
        // MasterVC roomTV is updated using loadRoomDetails and reloaded
        (NSApplication.sharedApplication().delegate as! AppDelegate).mainSplitVC.loadRoamDetails()
        // DetailsVC will not show any data as nothing will be selected in roomTV
        ((NSApplication.sharedApplication().delegate as! AppDelegate).mainSplitVC.detailVC.tabView.tabViewItemAtIndex(0).viewController as!  RoomDisplayVC).contentScrollView.documentView = nil
        //detailVC.tabView.tabViewItemAtIndex(0).viewController as!  RoomDisplayVC).contentScrollView.documentView = nil
        //AddRoomVC viewcontroller is dismissed
        self.dismissViewController(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.columnsTF.delegate = self
       self.rowsTF.delegate = self
        self.classNameTF.delegate = self
        
    }

}
