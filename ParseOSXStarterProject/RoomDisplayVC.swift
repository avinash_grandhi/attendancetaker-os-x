//
//  RoomDisplayVC.swift
//  AttendanceTakerOSX
//
//  Created by admin on 7/18/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import Cocoa

/****************************************************************************
This ViewController is instantiated when the user selects any room from rooms table view. *************************************************************************** */
class RoomDisplayVC: NSViewController, NSWindowDelegate {
    
    /* ****************************************************************************
    
    DetailVC(TabViewController)
    \
    |---------|--\------------------|
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|    RoomDisplayVC    |
    |      - +|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |      - +|                     |
    |---------|---------------------|
    
    
    
    ****************************************************************************
    Variables for the RoomDisplayVC
    *************************************************************************** */
    
    
    // reference to the MainSplitVC
    var mainSplitView : MainSplitVC!
    var roomData : Room!
    //Co-ordinates for the new Student.
    var newDeskAddPosition_x : CGFloat = 150
    var newDeskAddPosition_y : CGFloat = 250
    //Size of the desk.
    var deskSize: CGFloat  = 80
    // The below four variables are used to re-arrange the Students when window's size changes.
    var maxXCoordinate: CGFloat!
    var minXCoordinate : CGFloat!
    var minYCoordinate : CGFloat!
    var maxYcoordinate : CGFloat!
    
    @IBOutlet var fullContentView: NSView!
    @IBOutlet weak var saveBTN: NSButton!
    @IBOutlet weak var addDeskBTN: NSButton!
    @IBOutlet weak var contentScrollView: NSScrollView! // contentScrollView.documentView contains the desks (if it does).
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "unhighlightOtherDesks:", name: "unhighlightOtherDesks", object: nil)
    }
    
    /****************************************************************************
    When a user selects a desk, this method notifies other desks to unselect themselves.
    *************************************************************************** */
    func unhighlightOtherDesks(nsNotification:NSNotification){
    
        for view in contentScrollView.documentView!.subviews{
            (view ).layer!.backgroundColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1).CGColor
        }
    }
    
    
    override  func  viewDidDisappear() {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    /****************************************************************************
    Updating the status menu based on avilable functions
    *************************************************************************** */
    override func viewDidAppear() {
        // notification will be called when ther is any window resize
        //  NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("windowResize:"), name:NSWindowDidEndLiveResizeNotification, object: nil);
        if (self.mainSplitView != nil){
        (self.mainSplitView.splitViewItems[2] ).collapsed = true
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("windowResize:"), name:NSViewFrameDidChangeNotification , object: nil);
        //NSWindowDidResizeNotification
    }
    
    /****************************************************************************
    This function is called when there is any change is size of screen
    This calls the updateMaxMinDesksFrame function if the scroll view has desks
    *************************************************************************** */
    func windowResize(notification : NSNotification){
        
        if notification.object as! NSView == fullContentView{
            // loadContainer(Room)
            
            if contentScrollView.subviews[0].subviews[0].subviews.count > 0{
                updateMaxMinDesksFrame()
                
            }
        }
    }
    
    
    
    
    /* ****************************************************************************
    
    
    Scroll View
    |---------|--------------------/-
    |---------|                _    |
    |---------|               |_|---|----->Desk can be considered as top right point of the illusioned rectangle of desks
    |---------|                     |
    |---------|     _               |
    |---------|    |_|              |
    |      - +|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|     _               |
    |---------|    |_|--------------|----->Desk can be considered as bottom left point of the illusioned rectangle of desks
    |      - +|                     |
    |---------|---------------------|
    
    
    ****************************************************************************
    Updates the desks rectangle(illusion) max and min values based on scrollView content
    *************************************************************************** */
    
    func updateMaxMinDesksFrame(){
        let desk = contentScrollView.subviews[0].subviews[0].subviews[0] as! Desk
        maxXCoordinate = desk.frame.origin.x
        minXCoordinate  = desk.frame.origin.x
        minYCoordinate = desk.frame.origin.y
        maxYcoordinate  = desk.frame.origin.y
        
        var desksPoints : [NSPoint] = []
        
        // getting the values of the top left and bottom left coordinates
        for desks in contentScrollView.subviews[0].subviews[0].subviews{
            //store all the desks frame orgin and will be used to load the desks after window resize
            desksPoints.append(desks.frame.origin)
            if maxXCoordinate < desks.frame.origin.x{
                maxXCoordinate = desks.frame.origin.x
            }
            
            if minXCoordinate > desks.frame.origin.x{
                minXCoordinate = desks.frame.origin.x
            }
            
            if maxYcoordinate < desks.frame.origin.y{
                maxYcoordinate = desks.frame.origin.y
                
            }
            
            if minYCoordinate > desks.frame.origin.y{
                minYCoordinate = desks.frame.origin.y
            }
        }
        loadContainerAfterResize(desksPoints)
    }
    
    
    /* ****************************************************************************
    
    
    --------------------------|
    |                  <----->|
    |           RightDiffernce|
    |        ________         |
    |       |        |        |
    |       |        |        |
    |       |     ---|--------|---------->Desk views inside the illusioned rectangle
    |       |________|        |   |
    |LeftDiffernce            |
    |<---->             ------|---------->Scroll content view
    |_________________________|
    
    
    shiftDiffereceWidth = diffence of (leftDiffence and RightDifference)
    ****************************************************************************
    Get the shiftDifferenceWidth and shiftDifferenceHeight
    Load the desk views in the center of the scrollView using shiftDifferences
    Desk origin cooordinates are retrived from the calling fucntion
    *************************************************************************** */
    
    func loadContainerAfterResize(desksPoints : [NSPoint]){
        let contentview = NSView()
        contentview.setFrameOrigin(NSMakePoint(0, 0))
        // The container width and height of the desks if they were seen as placed in rectangle
        var desksWidth = (maxXCoordinate - minXCoordinate) + deskSize
        var desksHeight = maxYcoordinate - minYCoordinate + deskSize
        var shiftDifferenceWidth : CGFloat = 0
        var shiftDifferenceHeight : CGFloat = 0
        // calculating the deskwidth and shiftWidthDifference
        if self.contentScrollView.contentView.frame.width > desksWidth{
            desksWidth = self.contentScrollView.contentView.frame.width
            
            shiftDifferenceWidth = ((desksWidth - (maxXCoordinate + deskSize)) - (minXCoordinate))/2
        }
        else{
            shiftDifferenceWidth =  (minXCoordinate * -1)
        }
        // calculating the deskheight and shiftHeightDifference
        if (self.contentScrollView.contentView.frame.height ) > desksHeight{
            desksHeight = self.contentScrollView.contentView.frame.height
            shiftDifferenceHeight = ((desksHeight - (maxYcoordinate + deskSize)) - (minYCoordinate))/2
        }
        else{
            shiftDifferenceHeight = (minYCoordinate * -1)
        }
        contentview.setFrameSize(NSMakeSize(desksWidth, desksHeight))
        for i in 0..<desksPoints.count{
            let frame = NSMakeRect(desksPoints[i].x + shiftDifferenceWidth , desksPoints[i].y + shiftDifferenceHeight, 80.0, 80.0)
            let desk = Desk(frame: frame,newCell :false)
            contentview.addSubview(desk)
        }
        contentScrollView.documentView = contentview
    }
    
    
    
    /* ****************************************************************************
    
    
    Scroll View
    |---------|--------------------/-
    |---------|                _    |
    |---------|               |_|---|----->Desk can be considered as top right point of the illusioned rectangle of desks
    |---------|                     |
    |---------|     _               |
    |---------|    |_|              |
    |      - +|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|     _               |
    |---------|    |_|--------------|----->Desk can be considered as bottom left point of the illusioned rectangle of desks
    |      - +|                     |
    |---------|---------------------|
    
    
    ****************************************************************************
    Updates the desks rectangle(illusion) max and min values based on roomDetails data
    *************************************************************************** */
    
    func updateMaxMinFrameBasedOnRoomData(roomData: Room){
        if roomData.deskXaxis.count > 0{
            maxXCoordinate = roomData.deskXaxis[0]
            minXCoordinate = roomData.deskXaxis[0]
            minYCoordinate = roomData.deskYaxis[0]
            maxYcoordinate = roomData.deskYaxis[0]
        }
        for i in 0..<roomData.deskXaxis.count{
            if maxXCoordinate < roomData.deskXaxis[i]{
                maxXCoordinate = roomData.deskXaxis[i] as CGFloat
            }
            if minXCoordinate > roomData.deskXaxis[i]{
                minXCoordinate = roomData.deskXaxis[i] as CGFloat
            }
            if maxYcoordinate < roomData.deskYaxis[i]{
                maxYcoordinate = roomData.deskYaxis[i] as CGFloat
            }
            if minYCoordinate > roomData.deskYaxis[i]{
                minYCoordinate = roomData.deskYaxis[i] as CGFloat
            }
        }
    }
    /* ****************************************************************************
    
    
    --------------------------|
    |                  <----->|
    |           RightDiffernce|
    |        ________         |
    |       |        |        |
    |       |        |        |
    |       |     ---|--------|---------->Desk views inside the illusioned rectangle
    |       |________|        |   |
    |LeftDiffernce            |
    |<---->             ------|---------->Scroll content view
    |_________________________|
    
    
    shiftDiffereceWidth = diffence of (leftDiffence and RightDifference)
    ****************************************************************************
    Get the shiftDifferenceWidth and shiftDifferenceHeight
    Load the desk views in the center of the scrollView using shiftDifferences
    DeskViews are retrived from the calling function
    *************************************************************************** */
    
    @IBAction func loadContainer(sender: AnyObject) {
        
        if sender.isKindOfClass(Room){
            roomData = sender as! Room
            updateMaxMinFrameBasedOnRoomData(roomData)
            let contentview = NSView()
            contentview.setFrameOrigin(NSMakePoint(0, 0))
            // The container width and height of the desks if they were seen as placed in rectangle
            var desksWidth = (maxXCoordinate - minXCoordinate) + deskSize
            var desksHeight = maxYcoordinate - minYCoordinate + deskSize
            var shiftDifferenceWidth : CGFloat = 0
            var shiftDifferenceHeight : CGFloat = 0
            // calculating the deskwidth and shiftWidthDifference
            if self.contentScrollView.contentView.frame.width > desksWidth{
                desksWidth = self.contentScrollView.contentView.frame.width
                shiftDifferenceWidth = ((desksWidth - (maxXCoordinate + deskSize)) - (minXCoordinate))/2
            }
            else{
                shiftDifferenceWidth =  (minXCoordinate * -1)
            }
            // calculating the deskheight and shiftHeightDifference
            if (self.contentScrollView.contentView.frame.height ) > desksHeight{
                desksHeight = self.contentScrollView.contentView.frame.height
                shiftDifferenceHeight = ((desksHeight - (maxYcoordinate + deskSize)) - (minYCoordinate))/2
            }
            else{
                shiftDifferenceHeight = (minYCoordinate * -1)
            }
            contentview.setFrameSize(NSMakeSize(desksWidth, desksHeight))
            for i in 0..<(sender as! Room).deskXaxis.count{
                
                let frame = NSMakeRect(roomData.deskXaxis[i] + shiftDifferenceWidth , roomData.deskYaxis[i] + shiftDifferenceHeight, 80.0, 80.0)
                let desk = Desk(frame: frame,newCell :false)
                contentview.addSubview(desk)
                
            }
            contentScrollView.documentView = contentview
        }
    }
    
    
    /****************************************************************************
    Function to create click gesture recoginiser for deleting desks
    *************************************************************************** */
    
    func createGesture()-> NSClickGestureRecognizer{
        let clickgesture = NSClickGestureRecognizer()
        clickgesture.target = self
        clickgesture.numberOfClicksRequired = 1
        clickgesture.action = Selector("deleteDesk:")
        return clickgesture
    }
    
    /****************************************************************************
    Function to delete desks from the room
    On toggleClick1 assign the clickGestureRecogniser and cross image to all the desks in the room
    During the active clickGestureRecogniser on click of desk the desk is deleted
    On toggleClick2 removing the clickGestureRecogniser and cross image from all the desks in the room
    *************************************************************************** */
    
    @IBAction func deleteDesks(sender: NSButton) {
        if sender.state == 1{
            _ = NSClickGestureRecognizer()
            for desks in contentScrollView.subviews[0].subviews[0].subviews{
                desks.addGestureRecognizer(createGesture())
                let image = NSImageView()
                image.image = NSImage(named: "remove-151678_1280.png")
                image.setFrameSize(NSMakeSize(20, 20))
                image.setFrameOrigin(NSMakePoint(50, 50))
                desks.addSubview(image)
            }
            //disabling the buttons
            self.saveBTN.enabled = false
            self.addDeskBTN.enabled = false
            
        }
            //removing the clickGestureRecogniser and cross image from all the desks in the room
        else{
            for desks in contentScrollView.subviews[0].subviews[0].subviews{
                
                //removing the crossmark image from the desk
                for imageView in desks.subviews{
                    if (imageView as! NSImageView).image == NSImage(named: "remove-151678_1280.png"){
                        imageView.removeFromSuperview()
                    }
                }
                //removing the gesture from the desk
                for gesture in desks.gestureRecognizers{
                    if gesture.isKindOfClass(NSClickGestureRecognizer){
                        desks.removeGestureRecognizer(gesture as! NSClickGestureRecognizer)
                    }
                }
                // enabling the buttons
                self.saveBTN.enabled = true
                self.addDeskBTN.enabled = true
            }
        }
    }
    
    /****************************************************************************
    Function to delete desks from the room when a specific desk is clicked
    *************************************************************************** */
    func deleteDesk(ges : NSGestureRecognizer){
        let desk = ges.view
        desk?.removeFromSuperview()
    }
    
    
    /****************************************************************************
    Function to add desks to the room when a specific desk is clicked
    *************************************************************************** */
    
    @IBAction func addDesk(sender: AnyObject) {
        let frame = NSMakeRect(newDeskAddPosition_x  , newDeskAddPosition_y, deskSize, deskSize)
        let desk = Desk(frame: frame, newCell : true)
        NSAnimationContext.runAnimationGroup({(context)-> Void in
            context.duration = 4.0
            let newLabel = desk.subviews[1] as! NSImageView
            newLabel.animator().alphaValue = 0
            }, completionHandler: { () -> Void in
        })
        contentScrollView.documentView?.addSubview(desk)
    }
    
    /****************************************************************************
    Function to save desks to the room when a specific desk is clicked
    *************************************************************************** */
    
    @IBAction func saveContent(sender: AnyObject) {
        var deskXaxis : [CGFloat] = []
        var deskYaxis: [CGFloat] = []
        for desks in contentScrollView.subviews[0].subviews[0].subviews{
            deskXaxis.append(desks.frame.origin.x)
            deskYaxis.append(desks.frame.origin.y)
        }
        roomData.deskXaxis = deskXaxis
        roomData.deskYaxis = deskYaxis
        mainSplitView.saveClassRoom(roomData)
    }
    
    
    func saveV1(sender: AnyObject){
        saveContent(sender )
        print("saved")
    }
}
