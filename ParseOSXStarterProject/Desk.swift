//
//  Desk.swift
//  Sprint2
//
//  Created by admin on 7/5/15.
//  Copyright (c) 2015 admin. All rights reserved.
//

import Cocoa

/****************************************************************************
This class represents a Desk.
*************************************************************************** */

class Desk: NSView {
    override var acceptsFirstResponder : Bool { return true } // This will be the first object in the responder chain to be sent key events and action messages.
    var selected : Bool = false // To check if the desk is selected or not?
    let deskColor:NSColor = NSColor(red: 0.07, green: 0.57, blue: 0.64, alpha: 1) // Color of the desk
    var startingPanLocation:CGPoint!
    var name : NSString!
    var classDisplayVCDelegate : ClassDisplayVC!
    
    /****************************************************************************
    Instantiating the view based on the frame value and adding new image if it is newCell
    *************************************************************************** */
    init(frame frameRect: NSRect, newCell : Bool){
        super.init(frame: frameRect)
        let myPanGR:NSPanGestureRecognizer = NSPanGestureRecognizer(target: self, action: "moveMe:")
        self.addGestureRecognizer(myPanGR)
        loadImage()
        if newCell == true{
            loadNewImage()
        }
    }
    
    /****************************************************************************
    Loading the mickey image to the desk view
    *************************************************************************** */
    func loadImage(){
        let image = NSImageView()
        image.setFrameOrigin(NSMakePoint(0, 0))
        image.setFrameSize(NSMakeSize(100, 30))
        image.image = NSImage(named: "Mickey_Mouse.png")
        self.addSubview(image)
    }
    
    /****************************************************************************
    Loading the new sticker image to the desk view
    *************************************************************************** */
    
    func loadNewImage(){
        let image = NSImageView()
        image.setFrameOrigin(NSMakePoint(0, 15))
        image.setFrameSize(NSMakeSize(50, 50))
        image.image = NSImage(named: "new_red.png")
        self.addSubview(image)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /****************************************************************************
    overriding the drawrect function and filling the frame with color
    *************************************************************************** */
    
    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)
        let myBP:NSBezierPath = NSBezierPath(rect: NSMakeRect(5.0, 5.0, self.frame.size.width-10, self.frame.size.height-10))
        deskColor.setFill()  // desk's color (light turquoise)
        myBP.fill()
        
    }
    
    /****************************************************************************
    Adding the panGestureRecogniser to the Desk
    *************************************************************************** */
    
    func moveMe(myPanGR:NSPanGestureRecognizer){
        let deskIV = myPanGR.view!
        if myPanGR.state == NSGestureRecognizerState.Began {
            startingPanLocation = deskIV.frame.origin
        }
        deskIV.frame.origin.x = startingPanLocation.x + myPanGR.translationInView(self.superview).x
        deskIV.frame.origin.y = startingPanLocation.y + myPanGR.translationInView(self.superview).y
    }
    
    func unselect(){
        self.print("Unselected me, I am not worthy!")
    }
    
    /****************************************************************************
    Handling left mouse click. Toggle to specify if a desk is selected or not
    *************************************************************************** */
    
    override func mouseDown(theEvent: NSEvent) {
        NSNotificationCenter.defaultCenter().postNotificationName("unhighlightOtherDesks", object: self)
        // if !selected {
        self.layer?.backgroundColor = NSColor.redColor().CGColor
        selected = true
    }
    
    /****************************************************************************
    Handling keyboard events (51 = Delete button)
    *************************************************************************** */
    
    override func keyDown(theEvent: NSEvent) {
        if selected && theEvent.keyCode == 51 {
            self.removeFromSuperview()
        }
    }
}
