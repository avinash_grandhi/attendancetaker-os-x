//
//  AppDelegate.swift
//
//  Copyright 2011-present Parse Inc. All rights reserved.
//

import Cocoa

import Bolts
import ParseOSX

//Implicitly creates main for us. AppDelegate responses to all app-level events.
@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    @IBOutlet weak var statusMenu: NSMenu!
    var mainSplitVC : MainSplitVC!

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Enable storing and querying data from Local Datastore. 
        // Remove this line if you don't want to use Local Datastore features or want to use cachePolicy.
        Parse.enableLocalDatastore()

        /****************************************************************************
         Uncomment and fill in with your Parse credentials:
        ****************************************************************************/

        Parse.setApplicationId("WHJj4EU5OKo4UkXmCxaov34K86CljBJNdOmLw78u", clientKey: "0ayiXpFYMU62lh37blUYAlWVkZLEi9VNSaFG9Qhs")
        PFUser.enableAutomaticUser()
        let defaultACL: PFACL = PFACL()
        // If you would like to have all objects to be private by default, remove this line.
        defaultACL.setPublicReadAccess(true)
        defaultACL.setPublicWriteAccess(true)
        PFACL.setDefaultACL(defaultACL, withAccessForCurrentUser: true)
        
        /****************************************************************************
         Uncomment these lines to register for Push Notifications.
        
         let types = NSRemoteNotificationType.Alert |
                     NSRemoteNotificationType.Badge |
                     NSRemoteNotificationType.Sound;
         NSApplication.sharedApplication().registerForRemoteNotificationTypes(types)
         ****************************************************************************/

        PFAnalytics.trackAppOpenedWithLaunchOptions(nil)
    }

    func application(application: NSApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let installation = PFInstallation.currentInstallation()
        installation.setDeviceTokenFromData(deviceToken)
        installation.saveInBackground()

        PFPush.subscribeToChannelInBackground("") { (succeeded: Bool, error: NSError?) in
            if succeeded {
                print("ParseStarterProject successfully subscribed to push notifications on the broadcast channel.");
            } else {
                print("ParseStarterProject failed to subscribe to push notifications on the broadcast channel with error = %@.", error)
            }
        }
    }

    func application(application: NSApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("application:didFailToRegisterForRemoteNotificationsWithError: %@", error)
    }

    func application(application: NSApplication, didReceiveRemoteNotification userInfo: [String : AnyObject]) {
        PFAnalytics.trackAppOpenedWithRemoteNotificationPayload(userInfo)
    }

}

