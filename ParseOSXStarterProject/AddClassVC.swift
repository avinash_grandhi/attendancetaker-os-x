//
//  AddClassVC.swift
//  AttendanceTakerOSX
//
//  Created by admin on 7/23/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import Cocoa
import ParseOSX

/****************************************************************************
This ViewController is instantiated when the user clicks the Add class button (+) *************************************************************************** */

class AddClassVC: NSViewController {
    
    @IBOutlet weak var classNameTF: NSTextField!
    
    @IBOutlet weak var studentFileURLTF: NSTextField!
    
    @IBOutlet weak var roomNumberTF: NSTextField!
    
    var csvObject : CSVReader!
    var classData : Dictionary<String, [String]>!
    var roomDetailsArray : [Room]!
    var openPanel = NSOpenPanel()
    var fileLoaded : Bool = false
    var trackingArea : NSTrackingArea! // Defines a region of view that generates mouse-tracking events when the mouse is over that region.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do view setup here.
        roomDetailsArray = (NSApplication.sharedApplication().delegate as!AppDelegate).mainSplitVC.roomsDetailsArray
    }
    
    // This loads the .csv file with format lastname,firstname
    @IBAction func loadFile(sender: AnyObject) {
        openPanel.allowsMultipleSelection = false
        openPanel.canChooseDirectories = false
        openPanel.canCreateDirectories = false
        openPanel.canChooseFiles = true
        openPanel.runModal()
        let choosenFile : NSURL! = openPanel.URL
        
        if choosenFile != nil {
            studentFileURLTF.stringValue = choosenFile.description
            csvObject = try? CSVReader(contentsOfURL: choosenFile)
            classData = csvObject.columns
            fileLoaded = true
        }
    }
    
    
    
    @IBAction func addClass(sender: AnyObject) {
        if (!fileLoaded){
            let str : NSString = "file://" + studentFileURLTF.stringValue // need to append file:// because it doesn't show up when dragging ...
            if str != "" {
                let escapedStr = str.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
                if let url = NSURL(string: escapedStr!) {
                    csvObject = try? CSVReader(contentsOfURL: url)
                    classData = csvObject.columns
                } else
                {
                    print("In beta, we don't support spaces in urls (at least for this week)")
                }
            }
                
            else {
                print("AddClassVC: Dragging error")
            }
        }
        
        var roomAvailabilityCheck = 0
        var selectedRoom: Room!
        for room in roomDetailsArray{
            if roomAvailabilityCheck == 0{
                let compareResult : NSComparisonResult =   room.roomNumber.compare(roomNumberTF.stringValue)
                if compareResult.hashValue == 1{
                    roomAvailabilityCheck = 1
                    selectedRoom = room
                }
            }
        }
               if classData == nil{
            displayAlert("Please load the class Details file")
                   }
        else{
            classData["First name"] = refindeData(classData["First name"]!)
            classData["Email address"] = refindeData(classData["Email address"]!)
            classData["lastName"] = refindeData(classData["lastName"]!)
            Swift.print(classData["lastName"])
        }
        
        
        if classNameTF.stringValue == ""{
            displayAlert("Please enter class Name")
        }
        if roomAvailabilityCheck > 0 && classData != nil && classNameTF != ""{
            if classData["First name"]!.count < selectedRoom.deskXaxis.count{
                let emptyDeskCount = selectedRoom.deskXaxis.count - classData["First name"]!.count
                let emptyStartPostion = selectedRoom.deskXaxis.count - emptyDeskCount - 1
                for _ in emptyStartPostion...selectedRoom.deskXaxis.count{
                    classData["First name"]?.append("empty")
                    classData["Email address"]?.append("noSid")
                    classData["lastName"]?.append("last Name")
                    
                }
            }
            
            let attandancePresentArr = [Double] (count:selectedRoom.deskXaxis.count - 1,  repeatedValue: 100.0)
            let attandanceAbsentArr = [Double] (count:selectedRoom.deskXaxis.count - 1,  repeatedValue: 0.0)
            let attandanceLateArr = [Double] (count:selectedRoom.deskXaxis.count - 1,  repeatedValue: 0.0)
            
            let classRoomDataParse = PFObject(className:"ClassInfo")
            classRoomDataParse["name"] =  classNameTF.stringValue
            classRoomDataParse["deskXaxis"] = selectedRoom.deskXaxis
            classRoomDataParse["deskYaxis"] = selectedRoom.deskYaxis
            classRoomDataParse["studentNames"] = classData["First name"]
            classRoomDataParse["attandancePresentArr"] = attandancePresentArr
            classRoomDataParse["attandanceAbsentArr"] = attandanceAbsentArr
            classRoomDataParse["attandanceLateArr"] = attandanceLateArr
            classRoomDataParse["sid"] = classData["Email address"]
            classRoomDataParse["notes"] = classData["First name"]
            classRoomDataParse["studentSurnames"] = classData["lastName"]
            
            
            classRoomDataParse.saveInBackgroundWithBlock {
                (success: Bool, error: NSError?) -> Void in
                if (success) {
                    classRoomDataParse.saveInBackground()
                    // MasterVC roomTV is updated using loadRoomDetails and reloaded
                    
                    (NSApplication.sharedApplication().delegate as! AppDelegate).mainSplitVC.loadClassRooms()
                    
                    // DetailsVC will not show any data as nothing will be selected in roomTV
                    ((NSApplication.sharedApplication().delegate as! AppDelegate).mainSplitVC.detailVC.tabView.tabViewItemAtIndex(0).viewController as!  RoomDisplayVC).contentScrollView.documentView = nil
                    
                    //detailVC.tabView.tabViewItemAtIndex(0).viewController as!  RoomDisplayVC).contentScrollView.documentView = nil
                    //AddRoomVC viewcontroller is dismissed
                    self.dismissViewController(self)
                } else {
                    print("not saved in parse")
                    self.dismissViewController(self)

                }
            }
            
            
        }
            
        else {
            displayAlert("There is no room. Please create the room before creating class")
        }
    }
    
    /****************************************************************************
    Handle popups when any information is not provided as needed.
    *************************************************************************** */
    func displayAlert(message : NSString){
        let alert = NSAlert()
        alert.informativeText = message.description
        alert.messageText = "Message"
        alert.addButtonWithTitle("OK")
        alert.runModal()
    }
    
    //function to refine data
    
    func refindeData(array : [String])->[String]{
        var classDataRefined : [String] = []
        
        for line in array{
            if line != ""{
                classDataRefined.append(line)
            }
           
            
            
        }
         return classDataRefined
    }
}
