//
//  DetailVC.swift
//  AttendanceTakerOSX
//
//  Created by admin on 7/18/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import Cocoa

/****************************************************************************
View Controller for displaying selected class or room details.
*************************************************************************** */

class DetailVC: NSTabViewController {
    
    /* ****************************************************************************
    
    |---------|---------------------|
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|    DetailVC         |
    |      - +|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |---------|                     |
    |      - +|                     |
    |---------|---------------------|
    
    *************************************************************************** */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
}
