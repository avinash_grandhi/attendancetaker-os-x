//
//  TextField.swift
//  AttendanceTakerOSX
//
//  Created by admin on 9/17/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import Cocoa

/**************************************************************************
This class represents all the textfields in the AddClassVC.
*************************************************************************/

class TextField: NSTextField {
    
    override var acceptsFirstResponder : Bool { return true }
    
    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)
    }
    
    /**************************************************************************
    Defines a region of view that generates mouse-tracking events when the mouse is over that region. *************************************************************************/
    
    override func updateTrackingAreas() {
        
        super.updateTrackingAreas()
        
        self.addTrackingArea(NSTrackingArea(rect: self.bounds, options: [NSTrackingAreaOptions.EnabledDuringMouseDrag, NSTrackingAreaOptions.MouseEnteredAndExited, NSTrackingAreaOptions.ActiveAlways], owner: self, userInfo: nil))
        
    }
    
    /**************************************************************************
    Text field highlights and enables when the mouse enters that region specified by the tracking area.    *************************************************************************/
    
    override func mouseEntered(theEvent: NSEvent) {
        self.becomeFirstResponder()
    }
}
